
var refListUsed=document.getElementsByClassName('references-list')[1];
var refList=document.getElementsByClassName('references-list')[0];

var refUsedContainer=document.getElementsByClassName('references-used')[0];
var refContainer=document.getElementsByClassName('references-container')[0];


var nodelist = document.getElementsByClassName('reference');

var headReferenceUsed = document.getElementsByClassName('references-head')[1];
var headReferenceList = document.getElementsByClassName('references-head')[0];

//refList: lista de referencias de TODAS las referencias    display: none/block
//refListUsed: lista de referencias USADAS.					display: none/block

//refContainer: Contenedor de TODAS las referencias			height= 1.2em / 48% / 91%
//refUsedContainer: Contenedor de referencias USADAS		height= 1.2em / 47% / 97%

//add references to references scope
function addNewReferences(references){
	var referencesArray=references.split("@");
	var reflist=$("#refs").html();
	var idRef = (app.referenceList.length===1)?0:app.referenceList.length-1;
	for (var i = 1; i< referencesArray.length; i++) {
		var sample = bibtexParse.toJSON("@"+referencesArray[i]);
		if(!sample[0].entryTags.abstract) sample[0].entryTags.abstract="No abstract"
		app.referenceList.push(new rcmModel.Reference(idRef+i,sample[0].entryTags.title,sample[0].entryType,sample[0].entryTags.author,sample[0].entryTags.year,sample[0].entryTags.abstract))
        if(typeof sample[0].entryTags.author == 'undefined'){
            sample[0].entryTags.author = '';
        }
        if(typeof sample[0].entryTags.year == 'undefined'){
            sample[0].entryTags.year = '';
        }
		reflist=reflist + '<a href="javascript:void(0)"><div class="reference" id="'+(idRef+i)+'"onclick="app.addRef2Path(this)"><div class="colLeft"><div class="idRef">['+(idRef+i)+']</div><div class="yearRef">'+sample[0].entryTags.year+'</div></div><div class="colRight"><div class="titleRef">'+sample[0].entryTags.title+'</div><div class="authRef">'+sample[0].entryTags.author+'</div></div></div></a>'  
	}
	$("#refs").html( reflist);

}

function hiddenRefs() {
  	if(refList.style.display === "block") {
  		refList.style.display = "none";
  		refContainer.style.height="1.2em";
   		if (refListUsed.style.display === "block") {
   			refUsedContainer.style.height="95%";
   		}
   	}else {
   		refList.style.display = "block";
   		if (refListUsed.style.display!=="block") {
   			refContainer.style.height="91%";
   		}else{
   			refContainer.style.height="48%";
   		}
  	}  }


function hiddenUsedRefs(){
  	if(refListUsed.style.display === "block") {
   		refListUsed.style.display = "none";
   		refUsedContainer.style.height="1.2em";
   		if(refList.style.display==="block"){
   			refContainer.style.height="91%"
   		}
   	}else {
   		refListUsed.style.display = "block";
   		if (refList.style.display!=="block") {
   			refUsedContainer.style.height="97%"

   		}else{
   			refUsedContainer.style.height="47%";
   			refContainer.style.height="48%";
   		}
  	} 
 }



//aniadir al modelo gráfico el path
function addRefToLink(link,ref){
		var pathLink = link.get('path');
		pathLink.push(ref);//añado
		pathLink= _.uniq(pathLink);//elimino si es repetido
		pathLink= pathLink.sort();
		var pathLinkLab = "";
		if(pathLink.length!==0){
			pathLinkLab= '['+pathLink.join().replace(/a/g,'')+']';
		}

		link.set('path',pathLink); //lo guardo en la variable del link
		link.label(0,{ //pongo el label en el link
            position: .65,
            attrs:{
                rect:{fill:'white'},
                text:{fill:'black',text: pathLinkLab}
            }
    	});
}



//aniadir al modelo grafico el path
function addRefToElement(element,ref){
		var pathElement= element.get('path');
		pathElement.push(ref);
		pathElement= _.uniq(pathElement);
		element.set('path',pathElement);
 		if(element instanceof joint.shapes.basic.LinkingPhrases){
       		element.attr('text/fill','black');
        }else if(element instanceof joint.shapes.basic.Concept){
        	element.attr('rect/fill','#045FB4');
        }
}


//obtener la definición desde el modelo de rcm
function getDefinition(e){
        /*var idPath= e.id.replace(/[a-z]/g,'');
        idPath=idPath.replace("-",'')
        var suffixPath= e.id.replace(/[0-9]/g,'');
        suffixPath=suffixPath.replace("u-",'')
        if (suffixPath==='') {suffixPath=undefined};*/
		var idPath= e.id;
		var suffixPath= e.suffix;

		var path= rcmModelPathList.getPathsByIdAndSuffix(idPath,suffixPath);   
		if (path!==null){
			var syntagmsInPath= path.getSyntagms();
			console.log(syntagmsInPath);

			var syntagmList=''
			for (var i = 0; i < syntagmsInPath.length; i++) {
			    syntagmList=syntagmList+' '+rcmRCMNodesList.getLinkingPhrase(syntagmsInPath[i].getLinkingPhrase()).text+' '+rcmRCMNodesList.getConcept(syntagmsInPath[i].getConcept()).text
			}
			return (rcmRCMNodesList.getConcept(path.getFirstConcept()).text+syntagmList);
        }else{
        	alert("no existe path con ese id")
        }
}


//actualizar la lista de paths con el contenido de rcmModelPathList
GraphTool.prototype.addReference2PhatsList= function(){
	var newRef=''
	var pathList=rcmModelPathList.getPaths();
	for (var i = 0; i < pathList.length ; i++) {
		var id= (!pathList[i].getSuffix())?pathList[i].getID():pathList[i].getID()+''+pathList[i].getSuffix();
		var reference = pathList[i].getReference();
		var newRef=newRef+'<a href="javascript:void(0)">\
								<div class="referenceInPath reference" id="u-'+id+'">\
									<div class="colLeft">\
										<div class="idRef">'+id+'</div>\
									</div>\
									<div class="colRight">\
					                    <div class="titleRef">'+getDefinition(pathList[i])+'</div>\
									</div>\
								</div>\
								<img src="img/moins.png" id="del-'+id+'" class="del-ref" title="delete reference" onclick='+"delRef(this)"+'></img> \
							</a>'
	}
	refListUsed.innerHTML= newRef;
}


//añado ref a los elementos de una definicion
GraphTool.prototype.addRefTODefinition= function(e){
	if (app.elementsDefinition.length!==0) {
		if (app.elementsDefinition.length >=3) {
			var pathsById= rcmModelPathList.getPathsById(e.id); //vector de paths con un determinado ID (no tengo en cuenta el sufijo)
			var idComplet = (pathsById.length===0)?e.id:e.id+rcmModel.abecedario[pathsById.length]; //el identificador que quedará id+ sufijo
			var newPath = new rcmModel.Path(e.id,'',this.referenceList[e.id],app.elementsDefinition[0].id); //creo el nuevo objeto de tipo Path que se añadirá al modelo 
			rcmRCMNodesList.addConcept({"id":app.elementsDefinition[0].id,"text":app.elementsDefinition[0].attr("text/text")});
			for (var j = 0; j < app.elementsDefinition.length; j++) { //para ir añadiendo  tanto al elemento gráfico el identificador, ir creando los sintagmas y añadiendoselas al modelo RCM
				var element = app.elementsDefinition[j];
				addRefToElement(element,idComplet); //añado path al elemento gráfico
				if (j!==0 && j%2===1) {           //los linkingPhrase ocupan las posiciones impares
					rcmRCMNodesList.addConcept({"id":app.elementsDefinition[j+1].id,"text":app.elementsDefinition[j+1].attr("text/text")});
					rcmRCMNodesList.addLinkingPhrase({"id":app.elementsDefinition[j].id,"text":app.elementsDefinition[j].attr("text/text")});

					var newSyntagm= new rcmModel.Syntagm(app.elementsDefinition[j].id,app.elementsDefinition[j+1].id);
					newPath.addSyntagm(newSyntagm);
				}; 

				//asignar path a links que entran a un elemento
				var listLinks= app.graph.getConnectedLinks(element,{inbound: true}); //lista de links entrantes al elemento n
				for (var k = 0; k < listLinks.length; k++) {
					var encontrado= app.selection.where({id: listLinks[k].attributes.source.id}).length; //de los elementos seleccionados aquellos cullo id sea igual que el id_source del link
					if(encontrado){//TODO: darle una vuelta porque coje links que no debe en determinados bucles
						addRefToLink(listLinks[k],idComplet);
					}
				}
			}

			if (pathsById.length===0) { //no existe el path
				rcmModelPathList.addPath(newPath);
				//rcmModelUsedRefList.addReference(this.id,rcmModelReferenceList.getReference(this.id))//id, referencia			
			}else if (pathsById.length===1) { //corresponde con la primera repetición del path
				pathsById[0].setSuffix('a'); //cambio el identificador del primer elemento
				newPath.setSuffix('b');
				rcmModelPathList.addPath(newPath);
				//rcmModelUsedRefList.addReference(this.id+'a',rcmModelReferenceList.getReference(this.id))//id, referencia			
			}else{// n repeticiones siguientes
				newPath.setSuffix(rcmModel.abecedario[pathsById.length]);
				rcmModelPathList.addPath(newPath);
				//rcmModelUsedRefList.addReference(this.id+rcmModel.abecedario[pathsById.length],rcmModelReferenceList.getReference(this.id))//id, referencia uso el diccionario de rcmModel y sus posiciones para fijar la referencia
			}
			this.addReference2PhatsList();
		}else{
			$('.statusbar-container').text("El numero de elementos no es correcto").addClass('error');
            _.delay(
            	function() {
            		$('.statusbar-container').text('').removeClass('error')
            	}, 1500);
		}
	}else{
			$('.statusbar-container').text("No hay elementos seleccionados").addClass('error');
            _.delay(
            	function() {
            		$('.statusbar-container').text('').removeClass('error')
            	}, 1500);
    }
}


//función que se ejecuta cuando añadimos una nueva referencia
//TODO: hacer comprobación al inicializar el syntagma que los elementos impares son LinkingPhrase y los pares son Conceptos, al igual que el primer concepto
GraphTool.prototype.addRef2Path = function(e){
	
	if (this.selection.length!==0) {
		
		if (this.selection.length >=3) {
			
			var pathsById= rcmModelPathList.getPathsById(e.id); //vector de paths con un determinado ID (no tengo en cuenta el sufijo)
			var idComplet = (pathsById.length===0)?e.id+'a':e.id+rcmModel.abecedario[pathsById.length]; //el identificador que quedará id+ sufijo
			var newPath = new rcmModel.Path(e.id,'',this.referenceList[e.id],this.selection.at(0).id); //creo el nuevo objeto de tipo Path que se añadirá al modelo 
			rcmRCMNodesList.addConcept({"id":this.selection.at(0).id,"text":this.selection.at(0).attr("text/text")});
			for (var j = 0; j < this.selection.length; j++) { //para ir añadiendo  tanto al elemento gráfico el identificador, ir creando los sintagmas y añadiendoselas al modelo RCM
				
				var element = this.selection.at(j);
				addRefToElement(element,idComplet); //añado path al elemento gráfico
				if (j!==0 && j%2===1) {           //los linkingPhrase ocupan las posiciones impares
					rcmRCMNodesList.addConcept({"id":this.selection.at(j+1).id,"text":this.selection.at(j+1).attr("text/text")});
					rcmRCMNodesList.addLinkingPhrase({"id":this.selection.at(j).id,"text":this.selection.at(j).attr("text/text")});

					var newSyntagm= new rcmModel.Syntagm(this.selection.at(j).id,this.selection.at(j+1).id);
					newPath.addSyntagm(newSyntagm);
				}; 

				//asignar path a links que entran a un elemento
				var listLinks= this.graph.getConnectedLinks(element,{inbound: true}); //lista de links entrantes al elemento n
				for (var k = 0; k < listLinks.length; k++) {
					var encontrado= this.selection.where({id: listLinks[k].attributes.source.id}).length; //de los elementos seleccionados aquellos cullo id sea igual que el id_source del link
					if(encontrado){//TODO: darle una vuelta porque coje links que no debe en determinados bucles
						addRefToLink(listLinks[k],idComplet);
					}
				}
			}

			if (pathsById.length===0) { //no existe el path
				rcmModelPathList.addPath(newPath);
				//rcmModelUsedRefList.addReference(this.id,rcmModelReferenceList.getReference(this.id))//id, referencia			
			}else if (pathsById.length===1) { //corresponde con la primera repetición del path
				pathsById[0].setSuffix('a'); //cambio el identificador del primer elemento
				newPath.setSuffix('b');
				rcmModelPathList.addPath(newPath);

				//rcmModelUsedRefList.addReference(this.id+'a',rcmModelReferenceList.getReference(this.id))//id, referencia			
			}else{// n repeticiones siguientes
				newPath.setSuffix(rcmModel.abecedario[pathsById.length]);
				rcmModelPathList.addPath(newPath);
				//rcmModelUsedRefList.addReference(this.id+rcmModel.abecedario[pathsById.length],rcmModelReferenceList.getReference(this.id))//id, referencia uso el diccionario de rcmModel y sus posiciones para fijar la referencia
			}
			this.addReference2PhatsList();
			
		}else{
			$('.statusbar-container').text("El numero de elementos no es correcto").addClass('error');
            _.delay(
            	function() {
            		$('.statusbar-container').text('').removeClass('error')
            	}, 1500);
		}
	}else{
			$('.statusbar-container').text("No hay elementos seleccionados").addClass('error');
            _.delay(
            	function() {
            		$('.statusbar-container').text('').removeClass('error')
            	}, 1500);
    }
}



function delRefToElement(element,ref){
		var pathElement= element.get('path');
		pathElement= _.without(pathElement,ref);
		element.set('path',pathElement);
		if (pathElement.length===0) {
            if(element instanceof joint.shapes.basic.LinkingPhrases){
                element.attr('text/fill','red');
            }else if(element instanceof joint.shapes.basic.Concept){
                element.attr('rect/fill','#E76666');
            }
		}
}


function delRefToLink(link,ref){
		var pathLink = link.get('path');
		pathLink= _.without(pathLink,ref);
		link.set('path',pathLink);
		var pathLinkLab = "";
		if(pathLink.length!==0){
			pathLinkLab= '['+pathLink.join().replace(/a/g,'')+']';
		}
		link.label(0,{
            position:0.65,
            attrs:{
                rect:{fill:'white'},
                text:{fill:'black',text: pathLinkLab}
            }
    	});
}

var delRef= function(e){
		//asigno path a todos los elementos seleccionados y a sus enlaces
        var id=e.id.replace("del-",'')
        var arrayElements=[];
        if (app.selection.length===0) arrayElements= app.graph.getElements();
        else {
        	for (var i = 0; i < app.selection.length; i++) {arrayElements.push(app.selection.at(i))}
        }

		for (var i = 0; i < arrayElements.length; i++) {
			var element = arrayElements[i];
			delRefToElement(element,id);
			if(element.get('path').length===0){
				if(element instanceof joint.shapes.basic.LinkingPhrases){
	                element.attr('text/fill','red');
	            }else if(element instanceof joint.shapes.basic.Concept){
	                element.attr('rect/fill','#E76666');
	            }
			}

			//delete path from links
			var listLinks= app.graph.getConnectedLinks(element,{inbound: true}); //lista de links entrantes
			for (var j = 0; j < listLinks.length; j++) {
					delRefToLink(listLinks[j],id)
			}
		}
		rcmModelPathList.deletePath(id);
		addReference2PhatsList();//TODO: not exist is in rcmgraph prototype
}

/*
var addMode = function(){
	headReferenceList.style.background="#0B610B"
	for (var i = 0; i < nodelist.length; i++) {
		nodelist[i].onclick=addRef;
	};
}
*/
/*
var delMode = function(){
	headReferenceList.style.background="#B40404";
	for (var i = 0; i < nodelist.length; i++) {
		nodelist[i].onclick=delRef;
	};
}*/