
   var GraphTool = function(){
	   this.oAuthManage = new OAuthManage();
   };
   GraphTool.prototype={
		   
        home: function() {

            //inicio lista de referencias
            this.referenceList=[];
            this.referenceList.push(null);
        	
            this.initializeEditor();

            this.initializeRCMList();
            hiddenRefs();
            hiddenUsedRefs();
            
        },
      
        //dado un identificador y una definición, crea el path y lo añade a la lista de paths
        definition2RCM: function(idRef,def){
            var rtd= rcmAnalizer.split(def); //se obtiene {targetConcept:"",syntagmList[]}
            this.path2RCM(idRef,rtd);
        },
        
        //función que dado un identificador y un objeto {targetConcept:"Text",syntagmList[{concepText,LPText},...]} te crea el path y lo añade al modelo
        //dado un identificador y una definición, crea el path y lo añade a la lista de paths
        path2RCM: function(idRef,pathModel){
            var uniqID= joint.util.uuid();		  //obtención de identificador
            if(!rcmRCMNodesList.addConcept({"id":uniqID,"text":pathModel.targetConcept})){ //comprueba tanto id como text
                uniqID= rcmRCMNodesList.getConceptByText(pathModel.targetConcept);
            }

            var newPath= new rcmModel.Path(idRef,'',this.referenceList[idRef],uniqID); //Creación del path
            for (var i = 0; i < pathModel.syntagmList.length; i++) {
                var ConceptUniqID= joint.util.uuid();
                if(!rcmRCMNodesList.addConcept({"id":ConceptUniqID,"text":pathModel.syntagmList[i].concept})){
                    ConceptUniqID= rcmRCMNodesList.getConceptByText(pathModel.syntagmList[i].concept);
                }

                var LPUniqID= joint.util.uuid();
                if(!rcmRCMNodesList.addLinkingPhrase({"id":LPUniqID,"text":pathModel.syntagmList[i].linkingphrase})){
                    LPUniqID= rcmRCMNodesList.getLinkingPhraseByText(pathModel.syntagmList[i].linkingphrase);
                }

                var newSyntagm = new rcmModel.Syntagm(LPUniqID,ConceptUniqID); //creo el sintagma con el lp y concepto
                newPath.addSyntagm(newSyntagm);
            }
            rcmModelPathList.addPath(newPath); //finalmente añado el path
        },
       
        //implementada nueva
        /*	
		plotDefinition_deprecated: function(e){     
        	alert("plotDefinition invoked")
            var rtd= rcmAnalizer.split(e); //devuelve el targetConcept y una lista de sintagmas
            var rtdArray = e.split(' ');

            console.log('targetConcept: '+rtd.targetConcept)
            //console.log('syntagmList: '+rtd.syntagmList.length)
            this.selection.reset();                  //borro lo seleccionado anteriormente tanto del objeto como de la vista (selecion de cuadro)
            this.selectionView.cancelSelection();

            var newConcept= function(text){
                    var concept= new joint.shapes.basic.Concept({
                        size:{width:120,height:30},
                        attrs:{
                                rect: {rx: 2, ry: 2, width:25, height: 10,fill: '#045FB4'},
                                 text: { text: text, fill: 'black', 'font-size': 11, 'stroke-width': 0.5 }
                        },
                        path: []
                    });                    
                    var isConceptAll=false;                    //busqueda del concepto entre todos los elementos
                    for (var i = 0;!isConceptAll && i < allElements.length; i++) {
                        if(allElements[i] instanceof joint.shapes.basic.Concept && allElements[i].attr('text/text')===text){
                            isConceptAll=true;
                            concept=allElements[i]; //en caso de que exista se sustituye por el existente
                        }
                    }
                    return {"isBefore":isConceptAll,"concept":concept};
            }
            var newLinkingPhrase = function(text){
                    var linkingphrase = new joint.shapes.basic.LinkingPhrases({
                            size:{width:120,height:30},
                            attrs:{text: {text:text,'font-size': 14}},
                            path:[]                        
                    })
                        
                    var isLinkingPhrase=false;
                    for (var j = 0;!isLinkingPhrase && j < allElements.length; j++) {
                            if(allElements[j] instanceof joint.shapes.basic.LinkingPhrases && allElements[j].attr('text/text')===text){
                                        isLinkingPhrase=true;
                                        linkingphrase=allElements[j];
                           }
                    }
                    return {"isBefore":isLinkingPhrase,"linkingphrase":linkingphrase};
            }
            var selectionItems=[];

            if(rtd){ //
                    //Dibujo los resultados
                    //dibujo el concepto principal
                    var allElements= this.graph.getElements();
                    var elements=[]; //items que se van a añadir al editor

                    //creo el elemento concpeto primario
                    var concept= newConcept(rtd.targetConcept)


                    if(!concept.isBefore){elements.push(concept.concept);}   //si no existe el elemento se añade al vector para posteriormente pintarlo
                    selectionItems.push(concept.concept);                    //se seleccionará siempre


                    for (var i = 0; i < rtd.syntagmList.length; i++){

                        //creación del linkingphrase
                        var linkingphrase = newLinkingPhrase(rtd.syntagmList[i].linkingphrase);
                        //var inElements=false;
//                        for (var j = 0; j < elements.length; j++) {
//                            if (elements[j].attr('text/text')===linkingphrase.linkingphrase.attr('text/text')) {
//                                //alert(elements[j].attr('text/text')+"  vs "+linkingphrase.linkingphrase.attr('text/text'))
//                                inElements=true
//                            };
//                        };
                        //if(inElements){linkingphrase.isBefore=false;}                        
                        if(!linkingphrase.isBefore){ //|| inElements){
                            elements.push(linkingphrase.linkingphrase)
                        }
                        selectionItems.push(linkingphrase.linkingphrase);


                        //enlaza syntagms tras crear el LP  Concepto -> LP 
                        //concepto anterior con LP de ahora
                            var link = new joint.dia.Link({
                                    attrs: {
                                        // @TODO: scale(0) fails in Firefox
                                        //Para las flechas (grafo dirigido)
                                        //'.marker-source': {transform: 'scale(0.001)' },
                                        '.marker-target': {fill:'black', d: 'M 10 0 L 0 5 L 10 10 z' },
                                        '.connection-wrap': {
                                            stroke: 'black'
                                            //filter: { name: 'dropShadow', args: { dx: 1, dy: 1, blur: 2 } } //le pone una pequeña sombra
                                        }
                                    },
                                    smooth: (navigator.userAgent.indexOf('Firefox') !=-1)?false:true,
                                    path: [],
                                    source:{id:concept.concept.id},
                                    target:{id:linkingphrase.linkingphrase.id}})
                            var isNeighbor=false;
                            var connectedLinks= this.graph.getConnectedLinks(concept.concept,{outbound:true});
                            for (var j = 0; j < connectedLinks.length; j++) {
                                if( connectedLinks[j].get('target').id=== linkingphrase.linkingphrase.id){
                                    isNeighbor=true;
                                } }
                            if((!concept.isBefore && !linkingphrase.isBefore) || !isNeighbor){elements.push(link);}
                        //finaliza aqui 



                        //creación del concepto
                            var concept= newConcept(rtd.syntagmList[i].concept)
                            var isConceptElements=false;
                            for (var j = 0;!isConceptElements && j < elements.length; j++) {
                                if(elements[j] instanceof joint.shapes.basic.Concept && elements[j].attr('text/text')===rtd.syntagmList[i].concept){ 
                                    isConceptElements=true;
                                    concept.concept=elements[j]; //en caso de que exista se sustituye por el existente
                                }}
                            if(!concept.isBefore || !isConceptElements) elements.push(concept.concept)
                            selectionItems.push(concept.concept);
                        //finalización de la creacion de concepto


                        //enlaza el syntagma LP->Concept
                            var linkLPC = new joint.dia.Link(
                                {
                                    attrs: {
                                        // @TODO: scale(0) fails in Firefox
                                        //Para las flechas (grafo dirigido)
                                        //'.marker-source': {transform: 'scale(0.001)' },
                                        '.marker-target': {fill:'black', d: 'M 10 0 L 0 5 L 10 10 z' },
                                        '.connection-wrap': {
                                            stroke: 'black'
                                            //filter: { name: 'dropShadow', args: { dx: 1, dy: 1, blur: 2 } } //le pone una pequeña sombra
                                        }
                                    },
                                    smooth: (navigator.userAgent.indexOf('Firefox') !=-1)?false:true,
                                    path: [],
                                    source:{id:linkingphrase.linkingphrase.id},
                                    target:{id:concept.concept.id}
                                })
                            isNeighbor=false;
                            for (var j = 0; j < this.graph.getNeighbors(linkingphrase.linkingphrase).length; j++) {
                                if(this.graph.getNeighbors(linkingphrase.linkingphrase)[j].id === concept.concept.id){
                                    console.log("vecinos encontrados")
                                    isNeighbor=true;
                                } }
                            if((!concept.isBefore && !linkingphrase.isBefore) || !isNeighbor){
                            //alert("añado linkLPC")
                            elements.push(linkLPC)}
                        //finaliza aqui

                    }
                    this.graph.addCell(elements)
                    var elementsDefinition=selectionItems; //lista para añadir a definición ya que a partir de los seleccionados no puedo repeticiones

                    //seleccionar todos los elementos
                    for (var i = 0; i < selectionItems.length; i++) {
                        if (selectionItems[i] instanceof joint.shapes.basic.LinkingPhrases || selectionItems[i] instanceof joint.shapes.basic.Concept ){
                            var cellView= this.paper.findViewByModel(selectionItems[i])
                            this.selectionView.createSelectionBox(cellView);
                            this.selection.push(selectionItems[i]);
                        }
                    };
            }else{
                alert("no se ha podido parsear")
            }
            $('#btn-layout').click();
        },
*/

        newElement: function(element) {
            if (element instanceof joint.shapes.basic.LinkingPhrases || element instanceof joint.shapes.basic.Concept ){
                addElement= element;
                if(element instanceof joint.shapes.basic.LinkingPhrases && element.get('path').length===0){
                    element.attr('text/fill','red');
                }else if(element instanceof joint.shapes.basic.Concept && element.get('path').length===0){
                    element.attr('rect/fill','#E76666');
                }
            }
        },

        addTextElement: function (){
            var allElements= app.graph.getElements();
            var isElement= false;
            for (var i = 0;!isElement && i < allElements.length; i++) {
                if(allElements[i] instanceof joint.shapes.basic.Concept && allElements[i].attr('text/text')===$('#newelement').val()){ 
                    if($('#newelement').val()!=='') {
                        isElement=true
                    }
                }
            }
            if(!isElement){addElement.attr('text/text',$('#newelement').val());}
            else{
                alert("No se puede añadir porque el element ya existe"); 
                addElement.remove();
                addElement=undefined;
            }
            window.location.href = "#";
        },

        getRCM: function(name){
            var that = this;
            console.log("Pido: "+name);
            if(name && name!==""){            
                    $.ajax("/importGraph", 
                        {   "type": "post",   // usualmente post o get
                            "data": {"name":name},
                            "success": function(result) {
                                        that.addRCM(result);
                            },
                            "error": function(result) {
                                        console.error("Se ha producido un error: ", result);
                            },
                            "async": true,
                    });
            }
        },


        //used when import rcm model in JSON
        

        //coge el RCM y lo transforma en grafo para pintarlo
        
       
        /*importRCM: function(rcm){
                var RCMObject = JSON.parse(rcm);
                
                rcmModelPathList= new rcmModel.PathList();
                rcmRCMNodesList= new rcmModel.NodeList();

                document.getElementsByClassName('references-list')[1].innerHTML='';
                this.graph.clear();
                this.referenceList=[{}];
                var reflist="";
                for (var i = 0; i < RCMObject.pathlist.length; i++) {
                    //inicializo rcmModelPathList
                    var id= RCMObject.pathlist[i].id;
                    var suffix=RCMObject.pathlist[i].suffix;
                    //var reference= new rcmModel.Reference(RCMObject.pathlist[i].reference.id,RCMObject.pathlist[i].reference.title,RCMObject.pathlist[i].reference.type,RCMObject.pathlist[i].reference.authors,RCMObject.pathlist[i].reference.year,RCMObject.pathlist[i].reference.abstract);
                    var reference= new rcmModel.Reference(this.referenceList.length,RCMObject.pathlist[i].reference.title,RCMObject.pathlist[i].reference.type,RCMObject.pathlist[i].reference.authors,RCMObject.pathlist[i].reference.year,RCMObject.pathlist[i].reference.abstract);
                    var hasRef = false;
                    for(var j=1;j<this.referenceList.length;j++){
                        if (this.referenceList[j].title===reference.title) {
                            reference.id=this.referenceList[j].id;
                            hasRef=true;
                        }
                    }
                    if(!hasRef){
                        console.log("add ref: "+reference.id)
                        this.referenceList.push(reference);;
                        if(typeof reference.authors == 'undefined'){
                            reference.authors=[{first_name:"",last_name:""}];
                        }
                        reflist=reflist + '<a href="javascript:void(0)"><div class="reference" id="'+reference.id+'"onclick="addRef(this)"><div class="colLeft"><div class="idRef">['+(reference.id)+']</div><div class="yearRef">'+reference.year+'</div></div><div class="colRight"><div class="titleRef">'+reference.title+'</div><div class="authRef">'+reference.authors[0].first_name+' '+reference.authors[0].last_name+'</div></div></div></a>'  
                        $("#refs").html( reflist);                        
                    }

                    var firstConcept=RCMObject.pathlist[i].firstConcept;
                    var newPath = new rcmModel.Path(reference.id,'',reference,firstConcept)
                    for (var j = 0; j < RCMObject.pathlist[i].syntagmList.length; j++) {
                       var newSyntagm= new rcmModel.Syntagm(RCMObject.pathlist[i].syntagmList[j].linkingPhrase,RCMObject.pathlist[i].syntagmList[j].concept);
                       newPath.addSyntagm(newSyntagm);};
                    console.log(newPath)
                    rcmModelPathList.addPath(newPath);


                    //inicializo rcmModelNodelist
                    rcmRCMNodesList.conceptList= RCMObject.nodelist.conceptList;
                    rcmRCMNodesList.linkingPhraseList= RCMObject.nodelist.linkingPhraseList;           
                };
        },*/


        /*plotRCM: function(){
                var getElement = function(elementsDefinition,element){
                    for (var i = 0; i < elementsDefinition.length; i++) {
                        if(elementsDefinition[i].id===element.id) return elementsDefinition[i];
                    };
                    return null;}
                var existLink = function(plotElements,source,target){
                    for (var i = 0; i < plotElements.length; i++) {
                        if(plotElements[i].get('type')==="link" && plotElements[i].get('source').id===source && plotElements[i].get('target').id===target){
                            return plotElements[i]
                        } 
                    }
                    return null;}

                var plotElements= [];
                for (var i = 0; i < rcmModelPathList.getPaths().length; i++){
                    var numLinkingPhrase=0;
                    var pathID= rcmModelPathList.getPaths()[i].getAllID();

                    var concept = rcmRCMNodesList.getConcept(rcmModelPathList.getPaths()[i].getFirstConcept());
                    var conceptElement = getElement(plotElements,concept);
                    if(!conceptElement){
                        conceptElement= new joint.shapes.basic.Concept({
                            id:concept.id,
                            size:{width:120,height:30},
                            attrs:{
                                text: {text:concept.text,'font-size': 14},
                                rect: {rx: 2, ry: 2}
                            },
                            path: []});
                        conceptElement.set('path',[pathID])
                        plotElements.push(conceptElement);
                    }else{
                        var pathList = conceptElement.get('path');
                        pathList.push(pathID);
                        conceptElement.set('path',pathList);
                    }
                    conceptElement.attr('rect/fill','#609BB2');


                    var syntagmList= rcmModelPathList.getPaths()[i].getSyntagms();
                    for (var j = 0; j < syntagmList.length; j++) {

                        linkingPhrase = rcmRCMNodesList.getLinkingPhrase(syntagmList[j].getLinkingPhrase());
                        numLinkingPhrase= rcmModelPathList.getPaths()[i].numberLinkingPhrases(syntagmList[j].getLinkingPhrase());
                        var linkingphraseElement = getElement(plotElements,linkingPhrase);
                        if(!linkingphraseElement){
                            linkingphraseElement= new joint.shapes.basic.LinkingPhrases({
                                id:linkingPhrase.id,
                                size:{width:120,height:30},
                                attrs:{text: {text:linkingPhrase.text,'font-size': 14}},
                                path: []}
                            );
                            linkingphraseElement.set('path',[pathID])
                            plotElements.push(linkingphraseElement);
                        }else{
                            var pathList = linkingphraseElement.get('path');
                            pathList.push(pathID);
                            linkingphraseElement.set('path',pathList);
                        }
                        linkingphraseElement.attr('text/fill','black');


                        var linkCL = existLink(plotElements,conceptElement.id,linkingphraseElement.id)
                        if(!linkCL){ //si no existe creo el link con el path que le corresponde
                                linkCL = new joint.dia.Link({
                                    attrs: {
                                        // @TODO: scale(0) fails in Firefox
                                        //Para las flechas (grafo dirigido)
                                        //'.marker-source': {"transform": 'scale(0.001)' },
                                        '.marker-target': {fill:'black', d: 'M 10 0 L 0 5 L 10 10 z' },
                                        '.connection-wrap': {
                                            stroke: 'black'
                                            //filter: { name: 'dropShadow', args: { dx: 1, dy: 1, blur: 2 } } //le pone una pequeña sombra
                                        }
                                    },
                                    smooth: (navigator.userAgent.indexOf('Firefox') !=-1)?false:true,
                                    path: [pathID],//asigno path
                                    source:{id:conceptElement.id},
                                    target:{id:linkingphraseElement.id}
                                });
                                plotElements.push(linkCL)                     
                        }else{
                            var pathList = linkCL.get('path');
                            pathList.push(pathID);
                            linkCL.set('path',pathList);}
                        linkCL.label(0,{ //pongo el label en el link
                            position: .65,
                            attrs:{
                                rect:{fill:'white'},
                                text:{fill:'black',text: '['+linkCL.get('path').join()+']'}
                            }
                        });

                        concept =  rcmRCMNodesList.getConcept(syntagmList[j].getConcept());
                        var conceptElement = getElement(plotElements,concept);
                        if(!conceptElement){
                            conceptElement= new joint.shapes.basic.Concept({
                                id:concept.id,
                                size:{width:120,height:30},
                                attrs:{
                                    text: {text:concept.text,'font-size': 14},
                                    rect: {rx: 2, ry: 2}
                                },
                                path: []}
                            );
                            conceptElement.set('path',[pathID])
                            plotElements.push(conceptElement);
                        }else{
                            var pathList = conceptElement.get('path');
                            pathList.push(pathID);
                            conceptElement.set('path',pathList);}
                        conceptElement.attr('rect/fill','#609BB2');


                        var linkLC = existLink(plotElements,linkingphraseElement.id,conceptElement.id)
                        if(!linkLC){ //si no existe creo el link con el path que le corresponde
                                linkLC = new joint.dia.Link({
                                    attrs: {
                                        // @TODO: scale(0) fails in Firefox
                                        //Para las flechas (grafo dirigido)
                                        //'.marker-source': {"transform": 'scale(0.001)' },
                                        '.marker-target': {fill:'black', d: 'M 10 0 L 0 5 L 10 10 z' },
                                        '.connection-wrap': {
                                            stroke: 'black'
                                            //filter: { name: 'dropShadow', args: { dx: 1, dy: 1, blur: 2 } } //le pone una pequeña sombra
                                        }
                                    },
                                    smooth: (navigator.userAgent.indexOf('Firefox') !=-1)?false:true,
                                    path: [pathID],//asigno path
                                    source:{id:linkingphraseElement.id},
                                    target:{id:conceptElement.id}
                                });
                                plotElements.push(linkLC)                     
                        }else{
                            var pathList = linkLC.get('path');
                            pathList.push(pathID);
                            linkLC.set('path',pathList);
                        }
                        linkLC.label(0,{ //pongo el label en el link
                            position: .65,
                            attrs:{
                                rect:{fill:'white'},
                                text:{fill:'black',text: '['+linkLC.get('path').join()+']'}
                        }})
                    }
                }
            this.graph.addCells(plotElements);
            addReference2PhatsList();
            $('#btn-layout').click();
        },*/
   };
