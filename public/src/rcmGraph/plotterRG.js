
GraphTool.prototype.getCellbyText = function (text){
	var elements = this.graph.getElements();
	for (var i = elements.length-1; i >=0; i--) {
		var cell = elements[i];
		if(cell.get('attrs').text.text===text) return cell;	
	}
	return false;
},  

GraphTool.prototype.plotConcept =function(text, w){
            var concept= new joint.shapes.basic.Concept({
                size:{width:w*9,height:20},
                attrs:{
                        rect: {rx: 2, ry: 2, width:w, height: 20,fill: '#045FB4'},
                     text: { text: text, fill: 'black', 'font-size': 11, 'stroke-width': 0.5 }
            },
            path: []
        });                    
        var isConceptAll=false;                    //busqueda del concepto entre todos los elementos
        for (var i = 0;!isConceptAll && i < this.graph.getElements().length; i++) {
            if(this.graph.getElements()[i] instanceof joint.shapes.basic.Concept && this.graph.getElements()[i].attr('text/text')===text){
                isConceptAll=true;
                concept=this.graph.getElements()[i]; //en caso de que exista se sustituye por el existente
            }
        }
        if(!isConceptAll)this.graph.addCell(concept)     	
};
    
//función que pinta un linkingphrase, en caso de que exista no pintará nada
GraphTool.prototype.plotLinkingPhrase = function (text){
	var linkingphrase = new joint.shapes.basic.LinkingPhrases({
        size:{width:text.length*9,height:10},
        attrs:{text: {text:text,'font-size': 14}},
        path:[]                        
    })
    var isLinkingPhrase=false;
    for (var j = 0;!isLinkingPhrase && j < this.graph.getElements().length; j++) {
            if(this.graph.getElements()[j] instanceof joint.shapes.basic.LinkingPhrases && this.graph.getElements()[j].attr('text/text')===text){
            	var analizer = new RCMAnalizer();
        		if(analizer.pos(text).sentences[0].tokens[0].pos.parent==='verb'){
        				//alert(text+" is a verb");
            			isLinkingPhrase=true;
                        //linkingphrase=allElements[j];
        		}
            }
	}
    if(!isLinkingPhrase) this.graph.addCell(linkingphrase)   
};
        

GraphTool.prototype.plotArrow= function(source, target){
    var link = new joint.dia.Link({
        attrs: {
            // @TODO: scale(0) fails in Firefox
	        //Para las flechas (grafo dirigido)
	        //'.marker-source': {transform: 'scale(0.001)' },
        	'.marker-target': {fill:'black', d: 'M 10 0 L 0 5 L 10 10 z' },
        	'.connection-wrap': {
	            stroke: 'black'
	            //filter: { name: 'dropShadow', args: { dx: 1, dy: 1, blur: 2 } } //le pone una pequeña sombra
        	}
        },
	    smooth: (navigator.userAgent.indexOf('Firefox') !=-1)?false:true,
	    path: [],
	    source:{id:this.getCellbyText(source).id}, //TODO: cambiar para que se enlace bien
	    target:{id: this.getCellbyText(target).id}
	    }
    );

	var isNeighbor=false;
	var connectedLinks= this.graph.getConnectedLinks(this.getCellbyText(source),{outbound:true});//TODO: source es un elemento de joint
	for (var j = 0; j < connectedLinks.length; j++) {
	    if( connectedLinks[j].get('target').id=== this.getCellbyText(target).id){ //TODO: sustituir linkingphrase por target
            isNeighbor=true;
        } 
    }
    if(!isNeighbor){this.graph.addCell(link);}        	
};

//función que dibuja un sintagma, en caso de que exista no lo pintará
GraphTool.prototype.plotSyntagm= function(syntagm){
	this.plotConcept(syntagm.concept, syntagm.concept.length);
	this.plotLinkingPhrase(syntagm.linkingphrase);
	this.plotArrow(syntagm.linkingphrase, syntagm.concept);
};

//implementación nueva y más elegante
GraphTool.prototype.plotDefinition= function(e){    
	        	
	this.selection.reset();                  //borro lo seleccionado anteriormente tanto del objeto como de la vista (selecion de cuadro)
    this.selectionView.cancelSelection();

    var rtd= rcmAnalizer.split(e); //devuelve el targetConcept y una lista de sintagmas
    if(rtd){
    	this.plotConcept(rtd.targetConcept,rtd.targetConcept.length);
        for (var i = 0; i < rtd.syntagmList.length; i++){
        	this.plotSyntagm(rtd.syntagmList[i]);
        	if(i===0) {
        		this.plotArrow(rtd.targetConcept, rtd.syntagmList[i].linkingphrase);
        	}else{
        		this.plotArrow(rtd.syntagmList[i-1].concept, rtd.syntagmList[i].linkingphrase);
        	}
        }
        this.layoutDirectedGraph();
    }else{
    	 alert("no se ha podido parsear")
    }
    
};

//función que dibuja el modelo de la aplicación
GraphTool.prototype.plotRCM= function(){
    var plotElements= [];
    for (var i = 0; i < rcmModelPathList.getPaths().length; i++){
        var pathID= rcmModelPathList.getPaths()[i].getAllID();

        var concept = rcmRCMNodesList.getConcept(rcmModelPathList.getPaths()[i].getFirstConcept());
        var conceptElement = getElement(plotElements,concept);
        if(!conceptElement){
            conceptElement= new joint.shapes.basic.Concept({
                id:concept.id,
                size:{width:concept.text.length*9,height:20},
                attrs:{
                     rect: {rx: 2, ry: 2, width:25, height: 20,fill: '#045FB4'},
                     text: { text: concept.text, fill: 'black', 'font-size': 11, 'stroke-width': 0.5 }
                },
                path: []});
            conceptElement.set('path',[pathID])
            plotElements.push(conceptElement);
        }else{
            var pathList = conceptElement.get('path');
            pathList.push(pathID);
            conceptElement.set('path',pathList);
        }
        conceptElement.attr('rect/fill','#609BB2');


        var syntagmList= rcmModelPathList.getPaths()[i].getSyntagms();
        for (var j = 0; j < syntagmList.length; j++) {

            linkingPhrase = rcmRCMNodesList.getLinkingPhrase(syntagmList[j].getLinkingPhrase());
            var linkingphraseElement = getElement(plotElements,linkingPhrase);
            if(!linkingphraseElement){
                linkingphraseElement= new joint.shapes.basic.LinkingPhrases({
                    id:linkingPhrase.id,
                    size:{width:linkingPhrase.text.length*9,height:10},
                    attrs:{text: {text:linkingPhrase.text,'font-size': 14}},
                    path: []}
                );
                linkingphraseElement.set('path',[pathID])
                plotElements.push(linkingphraseElement);
            }else{
                var pathList = linkingphraseElement.get('path');
                pathList.push(pathID);
                linkingphraseElement.set('path',pathList);
            }
            linkingphraseElement.attr('text/fill','black');


            var linkCL = existLink(plotElements,conceptElement.id,linkingphraseElement.id)
            if(!linkCL){ //si no existe creo el link con el path que le corresponde
                    linkCL = new joint.dia.Link({
                        attrs: {
                            // @TODO: scale(0) fails in Firefox
                            //Para las flechas (grafo dirigido)
                            //'.marker-source': {"transform": 'scale(0.001)' },
                            '.marker-target': {fill:'black', d: 'M 10 0 L 0 5 L 10 10 z' },
                            '.connection-wrap': {
                                stroke: 'black'
                                //filter: { name: 'dropShadow', args: { dx: 1, dy: 1, blur: 2 } } //le pone una pequeña sombra
                            }
                        },
                        smooth: (navigator.userAgent.indexOf('Firefox') !=-1)?false:true,
                        path: [pathID],//asigno path
                        source:{id:conceptElement.id},
                        target:{id:linkingphraseElement.id}
                    });
                    plotElements.push(linkCL)                     
            }else{
                var pathList = linkCL.get('path');
                pathList.push(pathID);
                linkCL.set('path',pathList);}
            linkCL.label(0,{ //pongo el label en el link
                position: .65,
                attrs:{
                    rect:{fill:'white'},
                    text:{fill:'black',text: '['+linkCL.get('path').join().replace(/a/g,'')+']'}
                }
            });

            concept =  rcmRCMNodesList.getConcept(syntagmList[j].getConcept());
            var conceptElement = getElement(plotElements,concept);
            if(!conceptElement){
                conceptElement= new joint.shapes.basic.Concept({
                    id:concept.id,
                    size:{width:concept.text.length*9,height:20},
                    attrs:{
                    rect: {rx: 2, ry: 2, width:25, height: 20,fill: '#045FB4'},
                     text: { text: concept.text, fill: 'black', 'font-size': 11, 'stroke-width': 0.5 }
                    },
                    path: []}
                );
                conceptElement.set('path',[pathID])
                plotElements.push(conceptElement);
            }else{
                var pathList = conceptElement.get('path');
                pathList.push(pathID);
                conceptElement.set('path',pathList);
            }
            conceptElement.attr('rect/fill','#609BB2');


            var linkLC = existLink(plotElements,linkingphraseElement.id,conceptElement.id)
            if(!linkLC){ //si no existe creo el link con el path que le corresponde
                    linkLC = new joint.dia.Link({
                        attrs: {
                            // @TODO: scale(0) fails in Firefox
                            //Para las flechas (grafo dirigido)
                            //'.marker-source': {"transform": 'scale(0.001)' },
                            '.marker-target': {fill:'black', d: 'M 10 0 L 0 5 L 10 10 z' },
                            '.connection-wrap': {
                                stroke: 'black'
                                //filter: { name: 'dropShadow', args: { dx: 1, dy: 1, blur: 2 } } //le pone una pequeña sombra
                            }
                        },
                        smooth: (navigator.userAgent.indexOf('Firefox') !=-1)?false:true,
                        path: [pathID],//asigno path
                        source:{id:linkingphraseElement.id},
                        target:{id:conceptElement.id}
                    });
                    plotElements.push(linkLC)                     
            }else{
                var pathList = linkLC.get('path');
                pathList.push(pathID);
                linkLC.set('path',pathList);
            }
            linkLC.label(0,{ //pongo el label en el link
                position: .65,
                attrs:{
                    rect:{fill:'white'},
                    text:{fill:'black',text: '['+linkLC.get('path').join().replace(/a/g,'')+']'}
            }})
        }
    }
	app.graph.addCells(plotElements);
	$('#btn-layout').click();
}

GraphTool.prototype.plotNewDefinitionSet= function(e){
	var definitionsList= e;
	definitionsList= definitionsList.split("\n");
	for (var i = 0; i < definitionsList.length; i++) {
		var referencia = definitionsList[i].split("-");
		app.definition2RCM(referencia[0],referencia[1]);
	};
	var rcm = {"pathlist":rcmModelPathList.getPaths(),
			   "nodelist": {"conceptList":rcmRCMNodesList.getConceptList(),
				   			"linkingPhraseList":rcmRCMNodesList.getLinkingPhraseList()
				   			}
	}
	this.addRCM(JSON.stringify(rcm));
};




