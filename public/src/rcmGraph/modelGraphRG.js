var getElement = function(elementsDefinition,element){
    for (var i = 0; i < elementsDefinition.length; i++) {
        if(elementsDefinition[i].id===element.id) return elementsDefinition[i];
    };
    return null;
}

var existLink = function(plotElements,source,target){
    for (var i = 0; i < plotElements.length; i++) {
        if(plotElements[i].get('type')==="link" && plotElements[i].get('source').id===source && plotElements[i].get('target').id===target){
            return plotElements[i]
        } 
    }
    return null;
}

var importRCM= function(rcm){
    var RCMObject = JSON.parse(rcm);
    
    rcmModelPathList= new rcmModel.PathList();
    rcmRCMNodesList= new rcmModel.NodeList();

    document.getElementsByClassName('references-list')[1].innerHTML='';
    app.graph.clear();
    if(RCMObject.pathlist.length===0){ 
    	app.referenceList=[{}];
    	$("#refs").html('');
    }; //se eliminan las referencias al importar un "rcm" sin referencias
    var reflist="";
    for (var i = 0; i < RCMObject.pathlist.length; i++) {
        //inicializo rcmModelPathList
        var id= RCMObject.pathlist[i].id;
        var suffix=RCMObject.pathlist[i].suffix;
        //var reference= new rcmModel.Reference(RCMObject.pathlist[i].reference.id,RCMObject.pathlist[i].reference.title,RCMObject.pathlist[i].reference.type,RCMObject.pathlist[i].reference.authors,RCMObject.pathlist[i].reference.year,RCMObject.pathlist[i].reference.abstract);
        var reference= new rcmModel.Reference(app.referenceList.length,RCMObject.pathlist[i].reference.title,RCMObject.pathlist[i].reference.type,RCMObject.pathlist[i].reference.authors,RCMObject.pathlist[i].reference.year,RCMObject.pathlist[i].reference.abstract);
        var hasRef = false;
        for(var j=1;j<app.referenceList.length;j++){
            if (app.referenceList[j].title===reference.title) {
                reference.id=app.referenceList[j].id;
                hasRef=true;
            }
        }
        if(!hasRef){
            console.log("add ref: "+reference.id)
            app.referenceList.push(reference);;
            if(typeof reference.authors == 'undefined'){
            	reference.authors="";
//                reference.authors=[{first_name:"",last_name:""}];
            }
            reflist=reflist + '<a href="javascript:void(0)"><div class="reference" id="'+reference.id+'"onclick="app.addRef2Path(this)"><div class="colLeft"><div class="idRef">['+(reference.id)+']</div><div class="yearRef">'+reference.year+'</div></div><div class="colRight"><div class="titleRef">'+reference.title+'</div><div class="authRef">'+reference.authors+'</div></div></div></a>'  
            $("#refs").html( reflist);                        
        }

        var firstConcept=RCMObject.pathlist[i].firstConcept;
        var newPath = new rcmModel.Path(reference.id,'',reference,firstConcept)
        for (var j = 0; j < RCMObject.pathlist[i].syntagmList.length; j++) {
           var newSyntagm= new rcmModel.Syntagm(RCMObject.pathlist[i].syntagmList[j].linkingPhrase,RCMObject.pathlist[i].syntagmList[j].concept);
           newPath.addSyntagm(newSyntagm);
        };
        console.log(newPath)
        rcmModelPathList.addPath(newPath);


        //inicializo rcmModelNodelist
        rcmRCMNodesList.conceptList= RCMObject.nodelist.conceptList;
        rcmRCMNodesList.linkingPhraseList= RCMObject.nodelist.linkingPhraseList;           
    };
};


GraphTool.prototype.addRCM= function(rcm){
    importRCM(rcm);
    var svgObject = JSON.parse(rcm);
    console.log(svgObject.svgString)
    if(svgObject.svgString){
    	this.graph.fromJSON(JSON.parse(svgObject.svgString))
    }else{
    	this.plotRCM();
    }
	this.addReference2PhatsList();

    //this.plotRCM();
};