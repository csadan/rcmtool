
GraphTool.prototype.marcarPath= function(){
    path= $('#select-path').val();
    this.selectionView.cancelSelection();
    var allElements= this.graph.getElements();
    for (var i = 0; i < allElements.length; i++) {
        if (allElements[i].get('lastFill')) {
             allElements[i].attr('rect/fill',allElements[i].get('lastFill'));
             allElements[i].set('lastFill',null)
        }

        var elementPath = allElements[i].get('path');
        for (var j = 0;j < elementPath.length; j++) {
            if (elementPath[j]===path) {
                allElements[i].set('lastFill',allElements[i].attributes.attrs.rect.fill);
                allElements[i].attr('rect/fill',"#80FF00")
            }
        }
    }
};

GraphTool.prototype.openAsPNG= function() {
    
    var windowFeatures = 'menubar=yes,location=yes,resizable=yes,scrollbars=yes,status=yes';
    var windowName = _.uniqueId('png_output');
    var imageWindow = window.open('', windowName, windowFeatures);
    this.paper.toPNG(function(dataURL){
       imageWindow.document.write('<img src="' + dataURL + '"/>');
    }, { padding: 10 });
};

GraphTool.prototype.toggleFullscreen=function() {
    var el = document.body;
      if(el.requestFullScreen) {
        el.requestFullScreen();
      } else if(el.mozRequestFullScreen) {
        el.mozRequestFullScreen();
      } else if(el.webkitRequestFullScreen(Element.ALLOW_KEYBOARD_INPUT)){
        el.webkitRequestFullScreen(Element.ALLOW_KEYBOARD_INPUT);
      }
      setTimeout(function() {
        if (!document.webkitCurrentFullScreenElement && el.webkitRequestFullScreen()) {
          el.webkitRequestFullScreen();
        }
      },100);
    /*
    function prefixedResult(el, prop) {
        var prefixes = ['webkit', 'moz', 'ms', 'o', ''];
        for (var i = 0; i < prefixes.length; i++) {
            var prefix = prefixes[i];
            var propName = prefix ? (prefix + prop) : (prop.substr(0, 1).toLowerCase() + prop.substr(1));
            if (!_.isUndefined(el[propName])) {
                return _.isFunction(el[propName]) ? el[propName]() : el[propName];
            }
        }
    }

    if (prefixedResult(document, 'FullScreen') || prefixedResult(document, 'IsFullScreen')) {
        prefixedResult(document, 'CancelFullScreen');
    } else {
        prefixedResult(el, 'RequestFullScreen');
    }*/
};

GraphTool.prototype.graph2RCM= function(){
    var sendRCM = {"pathlist":rcmModelPathList.getPaths(),
		    		"nodelist": {"conceptList":rcmRCMNodesList.getConceptList(),
		    					 "linkingPhraseList":rcmRCMNodesList.getLinkingPhraseList()}}

    var svgRCM = JSON.stringify(this.graph.toJSON());  
    console.log("nombre: "+$('#nameRCM').val()+"enviado rcm"+sendRCM);
    var name = $('#nameRCM').val();
    sendRCM.svgString= svgRCM;
    $.post( "/saveGraph", {"name": name ,"jsonString": JSON.stringify(sendRCM)}) ;
    this.getRCMList();
};

GraphTool.prototype.zoomOut= function(){ 
	this.zoom((this.zoomLevel || 1) - 0.2); 
};

GraphTool.prototype.zoomIn= function(){ 
	this.zoom((this.zoomLevel || 1) + 0.2); 
};

GraphTool.prototype.getGridBackgroundImage= function(gridSize, color) {

    var canvas = $('<canvas/>', { width: gridSize, height: gridSize });

    canvas[0].width = gridSize;
    canvas[0].height = gridSize;
    var context = canvas[0].getContext('2d');
    context.beginPath();
    context.rect(1, 1, 1, 1);
    context.fillStyle = color || '#AAAAAA';
    context.fill();

    return canvas[0].toDataURL('image/png');
};

GraphTool.prototype.setGrid=function(gridSize) {

    this.paper.options.gridSize = gridSize;
    
    var backgroundImage = this.getGridBackgroundImage(gridSize);
    $(this.paper.svg).css('background-image', 'url("' + backgroundImage + '")');
};



GraphTool.prototype.layoutDirectedGraph= function() {
    this.commandManager.initBatchCommand();
    _.each(this.graph.getLinks(), function(link) {
        // Reset vertices.
        link.set('vertices', []);
        
        // Remove all the non-connected links.
        if (!link.get('source').id || !link.get('target').id) {
            link.remove();
        }
    });
    
    joint.layout.DirectedGraph.layout(this.graph,{setLinkVertices: false, rankDir: 'LR', nodeSep:50, edgeSep:50, rankSep:50});
    
    this.paperScroller.el.scrollLeft =0;
    
    this.paperScroller.el.scrollTop = 0;
    
    this.commandManager.storeBatchCommand();
};
GraphTool.prototype.zoom= function(newZoomLevel, ox, oy) {

    if (_.isUndefined(this.zoomLevel)) { this.zoomLevel = 1; }

    if (newZoomLevel > 0.2 && newZoomLevel < 20) {

    ox = ox || (this.paper.el.scrollLeft + this.paper.el.clientWidth / 2) / this.zoomLevel;
    oy = oy || (this.paper.el.scrollTop + this.paper.el.clientHeight / 2) / this.zoomLevel;

    this.paper.scale(newZoomLevel, newZoomLevel, ox, oy);

    this.zoomLevel = newZoomLevel;
    }
};


GraphTool.prototype.getDefinition= function(){
    path= $('#select-path').val();
    if(path.length===1) path=path+'a';
    pathElement=rcmModelPathList.getPathsByIdAndSuffix(path.charAt(0),path.charAt(1));
    if(!pathElement){
        alert("No se ha encontrado la definición para el path")
    }else{
        alert(pathElement.getDefinition())
    }
};