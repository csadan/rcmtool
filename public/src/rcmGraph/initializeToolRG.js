GraphTool.prototype.initializeRCMList = function() {
	this.getRCMList();
};

GraphTool.prototype.initializeReferenceList = function() {
	this.oAuthManage.getDocuments(); //oauth.js
};

GraphTool.prototype.initializeEditor = function() {

	this.inspectorClosedGroups = {};

	this.initializePaper();
	this.initializeStencil();
	this.initializeSelection();

	this.initializeHaloAndInspector();
	this.initializeClipboard(); //TODO: falla ctrl+c y ctr+v
	this.initializeCommandManager();
	this.initializeToolbar();
	// Intentionally commented out. See the `initializeValidator()` method for reasons.
	// Uncomment for demo purposes.
	this.initializeValidator();
	// Commented out by default. You need to run `node channelHub.js` in order to make
	// channels working. See the documentation to the joint.com.Channel plugin for details.
	//this.initializeChannel('ws://localhost:4141');
	//if (this.options.channelUrl) {
	//    this.initializeChannel(this.options.channelUrl);
	//}
};

// Create a graph, paper and wrap the paper in a PaperScroller.
GraphTool.prototype.initializePaper = function() {

	
			this.graph = new joint.dia.Graph;
	
			
			
			//################################# resize rect when text change ###################################################
			//https://groups.google.com/forum/#!searchin/jointjs/resize$20element/jointjs/vKBbVsuGc8E/IoPagkRCgYYJ
			//change size of cell
			this.graph.on("change:attrs",_.bind(
					function(cell, attrs, opt) {
					if (cell.previousAttributes().attrs.text
							&& attrs.text
							&& cell.previousAttributes !== ''
							&& attrs.text !== '') {
						if (cell
								.previousAttributes().attrs.text.text != attrs.text.text) { //test if label changed
							//alert("id:"+cell.id+" texto: "+attrs.text.text+" cellType: "+cell.attributes.type);

							if (cell.attributes.type === "basic.Concept") {//cambio información de concepto en el modelo 
								if (!rcmRCMNodesList.hasConceptByText(attrs.text.text)) {
									if (rcmRCMNodesList.hasConcept(cell.id))
										rcmRCMNodesList.setConcept(cell);
								} else {
									alert("El concepto ya existe")
									cell.attr('text/text',cell.previousAttributes().attrs.text.text)
									var changeSize = true;
								}
							} else if (cell.attributes.type === "basic.LinkingPhrases") {//cambio información de link en el modelo
								if (rcmRCMNodesList
										.hasLinkingPhrase(cell.id)) {
									rcmRCMNodesList
											.setLinkingPhrase(cell);
								}
							}

							if (!changeSize) {
								var maxLineLength = _
										.max(
												attrs.text.text
														.split('\n'),
												function(
														l) {
													return l.length;
												}).length;

								// Compute width/height of the rectangle based on the number
								// of lines in the label and the letter size. 0.6 * letterSize is
								// an approximation of the monospace font letter width.
								var letterSize = 14;
								var width = (letterSize * (0.6 * maxLineLength + 1));
								var height = ((attrs.text.text
										.split('\n').length + 1) * letterSize);
								cell
										.set({
											size : {
												width : width,
												height : height
											}
										})
							}
						}
					}
				}, this));

			this.paperScroller = new joint.ui.PaperScroller({
				autoResizePaper : true
			});
			var defaultLinks = new joint.dia.Link({
				attrs : {
					// @TODO: scale(0) fails in Firefox
					//Para las flechas (grafo dirigido)
					//'.marker-source': {transform: 'scale(0.001)' },
					'.marker-target' : {
						fill : 'black',
						d : 'M 10 0 L 0 5 L 10 10 z'
					},
					'.connection-wrap' : {
						stroke : 'black'
					//filter: { name: 'dropShadow', args: { dx: 1, dy: 1, blur: 2 } } //le pone una pequeña sombra
					}
				},
				smooth : (navigator.userAgent.indexOf('Firefox') != -1) ? false
						: true,
				path : []
			});
			/*defaultLinks.on('change:source',function(){
			    alert("change source")
			});
			defaultLinks.on('change:target',function(){
			    alert("change source")
			});*/

			this.paper = new joint.dia.Paper(
					{
						el : this.paperScroller.el,
						width : 1100,
						height : 1100,
						gridSize : 10,
						perpendicularLinks : true,
						model : this.graph,
						defaultLink : defaultLinks,
						validateConnection : function(cellViewS, magnetS,
								cellViewT, magnetT, end, linkView) {
							// Only rectangle can be the source, if it is not, we do not allow such connection:
							if (cellViewS.model.get('type') === 'basic.LinkingPhrases'
									&& cellViewT.model.get('type') === 'basic.Concept') {
								$('.statusbar-container').text(
										'Correct new link').addClass('correct');
								_.delay(function() {
									$('.statusbar-container').text('')
											.removeClass('correct');
								}, 1500);
								return true;
							}
							;
							if (cellViewS.model.get('type') === 'basic.Concept'
									&& cellViewT.model.get('type') === 'basic.LinkingPhrases') {
								$('.statusbar-container').text(
										'Correct new link').addClass('correct');
								_.delay(function() {
									$('.statusbar-container').text('')
											.removeClass('correct');
								}, 1500);
								return true;
							}
							;
							if (cellViewS.model.get('type') === 'basic.Concept'
									&& cellViewT.model.get('type') === 'basic.Concept') {
								return false;
							}
							;
							if (cellViewS.model.get('type') === 'basic.LinkingPhrases'
									&& cellViewT.model.get('type') === 'basic.LinkingPhrases') {
								return false;
							}
							;
							return false;
						}
					});

			
			this.paperScroller.options.paper = this.paper;

			$('.paper-container').append(this.paperScroller.render().el);
			//this.paperScroller.center();

			this.graph.on('add', this.newElement, this); //añadir un nuevo elemento al grafo

			// Uncomment for zoom by scrolling:
			$('.paper-scroller')
					.on(
							'mousewheel DOMMouseScroll',
							_
									.bind(
											function(evt) {
												if (evt.ctrlKey) {
													evt.preventDefault();
													var delta = Math
															.max(
																	-1,
																	Math
																			.min(
																					1,
																					(evt.originalEvent.wheelDelta || -evt.originalEvent.detail)));
													delta = delta / 100;
													this
															.zoom(
																	(this.zoomLevel || 1)
																			+ delta,
																	evt.clientX,
																	evt.clientY);
												}
											}, this));
		};

GraphTool.prototype.initializeStencil= function() {

    this.stencil = new joint.ui.Stencil({ graph: this.graph, paper: this.paper, width: 100, groups: Stencil.groups });
    $('.stencil-container').append(this.stencil.render().el);//se carga el stencil en el elemento con id .stencil-container

    _.each(Stencil.groups, function(group, name) {
        this.stencil.load(Stencil.shapes[name], name); //agrega la lista de elementos en este caso sólo basic
        joint.layout.GridLayout.layout(this.stencil.getGraph(name),{ //layout dentro del stencil (en definitiva es otro grafo)
            columnWidth: this.stencil.options.width,
            columns: 2,
            rowHeight: 70,
            resizeToFit: true,
            dy: 10,
            dx: 10
        });
        this.stencil.getPaper(name).fitToContent(1, 1, 10);
    }, this);
    $('.stencil-container .btn-expand').on('click', _.bind(this.stencil.openGroups, this.stencil));       //eventos para abrir los grupos o cerrarlos
    $('.stencil-container .btn-collapse').on('click', _.bind(this.stencil.closeGroups, this.stencil));

    this.initializeStencilTooltips();
};

GraphTool.prototype.initializeStencilTooltips= function() {
    // Create tooltips for all the shapes in stencil.
    _.each(this.stencil.graphs, function(graph){
        graph.get('cells').each(function(cell) {
            new joint.ui.Tooltip({
                target: '.stencil [model-id="' + cell.id + '"]',
                content: 'arrastrar elemento para introducir en el grafo',//cell.get('type').split('.').join(' '),
                left: '.stencil',
                direction: 'left'
            });
        });
    });
};

GraphTool.prototype.initializeSelection= function() {
    
    this.selection = new Backbone.Collection;
    this.selectionView = new joint.ui.SelectionView({ paper: this.paper, graph: this.graph, model: this.selection, filter: ['joint.dia.Link'] });

    // Initiate selecting when the user grabs the blank area of the paper while the Shift key is pressed.
    // Otherwise, initiate paper pan.
    this.paper.on('blank:pointerdown', function(evt, x, y) {
        if (_.contains(KeyboardJS.activeKeys(), 'shift')) {
            this.selectionView.startSelecting(evt, x, y);
        } else {
            if(this.inspector){
            	this.inspector.remove();
            	$("#changeAtrr").hide();
            }
            this.selectionView.cancelSelection();
            this.paperScroller.startPanning(evt, x, y);
        }
    }, this);


    this.paper.on('cell:pointerclick', function(cellView, evt) {
        // Select an element if CTRL/Meta key is pressed while the element is clicked.
        //if (!(evt.ctrlKey || evt.metaKey)) {
                if (this.selection.length!==0) {
                    var conLinks= this.graph.getConnectedLinks(cellView.model,{inbound: true});
                    var isConnected=false;
                    for (var i = 0; i < conLinks.length && !isConnected; i++) {
                       var numConLink= this.selection.where({id: conLinks[i].attributes.source.id}).length;
                       (numConLink>0)?isConnected=true:isConnected=false;
                    }
                    if (isConnected) {
                        this.selectionView.createSelectionBox(cellView);
                        this.selection.add(cellView.model);
                    }
                }else{ 
                    var conLinks= this.graph.getConnectedLinks(cellView.model,{inbound: true});
                    if (conLinks.length===0){
                        this.selectionView.createSelectionBox(cellView);
                        this.selection.add(cellView.model);
                    }
                }
        //}
    }, this);

    this.selectionView.on('selection-box:pointerclick', function(evt) {
            var cell = this.selection.get($(evt.target).data('model'));
            this.selectionView.destroySelectionBox(this.paper.findViewByModel(cell));
            this.selection.reset(this.selection.without(cell));
    }, this);

    // Disable context menu inside the paper.
    // This prevents from context menu being shown when selecting individual elements with Ctrl in OS X.
    this.paper.el.oncontextmenu = function(evt) { evt.preventDefault(); };

    KeyboardJS.on('delete, backspace', _.bind(function(evt) {

        if (!$.contains(evt.target, this.paper.el)) {
            // remove selected elements from the paper only if the target is the paper
            return;
        }
        this.commandManager.initBatchCommand();
        this.selection.invoke('remove');
        this.commandManager.storeBatchCommand();
        this.selectionView.cancelSelection();
    }, this));
};

GraphTool.prototype.createInspector= function(cellView) {
    // No need to re-render inspector if the cellView didn't change.
   // if (!this.inspector || this.inspector.options.cellView !== cellView) {
        
        if (this.inspector) {

            this.inspectorClosedGroups[this.inspector.options.cellView.model.id] = _.map(app.inspector.$('.group.closed'), function(g) { return $(g).index() });
            
            // Clean up the old inspector if there was one.
            this.inspector.remove();
            $("#changeAtrr").hide();
        }

        var inspectorDefs = InspectorDefs[cellView.model.get('type')];

        this.inspector = new joint.ui.Inspector({
            inputs: inspectorDefs ? inspectorDefs.inputs : CommonInspectorInputs,
            groups: inspectorDefs ? inspectorDefs.groups : CommonInspectorGroups,
            cellView: cellView
        });

        this.initializeInspectorTooltips();
        
        this.inspector.render();
        $('.inspector-container').html(this.inspector.el);
        $("#changeAtrr").show();

        if (this.inspectorClosedGroups[cellView.model.id]) {
            _.each(this.inspector.$('.group'), function(g, i) {
                if (_.contains(this.inspectorClosedGroups[cellView.model.id], $(g).index())) {
                    $(g).addClass('closed');
                }
            }, this);
        } else {
            this.inspector.$('.group:not(:first-child)').addClass('closed');
        }
    //}
};

GraphTool.prototype.initializeInspectorTooltips= function() {
    
    this.inspector.on('render', function() {
        this.inspector.$('[data-tooltip]').each(function() {
            var $label = $(this);
            new joint.ui.Tooltip({
                target: $label,
                content: $label.data('tooltip'),
                right: '.inspector',
                direction: 'right'
            });
        });
        
    }, this);
};

/*función que se ejecuta si se ha pulsado la tecla control al hacer pointerdown sobre una celda*/
GraphTool.prototype.initializeHaloAndInspector = function() {
	this.paper.on('cell:pointerclick', function(cellView, evt, x, y) {
		if ((evt.ctrlKey || evt.metaKey)) {

			// In order to display halo link magnets on top of the
			// freetransform div we have to create the
			// freetransform first. This is necessary for IE9+ where
			// pointer-events don't work and we wouldn't
			// be able to access magnets hidden behind the div.
			var freetransform = new joint.ui.FreeTransform({
				graph : this.graph,
				paper : this.paper,
				cell : cellView.model
			});
			
			var halo = new joint.ui.Halo({
				graph : this.graph,
				paper : this.paper,
				cellView : cellView
			});
			halo.removeHandle('rotate');
			halo.removeHandle('clone');
			halo.removeHandle('resize');
			halo.removeHandle('fork');

			freetransform.render();
			halo.render();

			this.initializeHaloTooltips(halo);

			this.createInspector(cellView);
			$("#changeAtrr").show();
			this.selectionView.cancelSelection();
			this.selection.reset([ cellView.model ]);
		}
	}, this);


    this.paper.on('link:options', function(evt, cellView, x, y) {
        this.createInspector(cellView);
        $("#changeAtrr").show();
    }, this);
};

GraphTool.prototype.initializeHaloTooltips= function(halo) {

    new joint.ui.Tooltip({
        className: 'tooltip small',
        target: halo.$('.remove'),
        content: 'Click to remove the object',
        direction: 'right',
        right: halo.$('.remove'),
        padding: 15
    });
    new joint.ui.Tooltip({
        className: 'tooltip small',
        target: halo.$('.fork'),
        content: 'Click and drag to clone and connect the object in one go',
        direction: 'left',
        left: halo.$('.fork'),
        padding: 15
    });
    new joint.ui.Tooltip({
        className: 'tooltip small',
        target: halo.$('.clone'),
        content: 'Click and drag to clone the object',
        direction: 'left',
        left: halo.$('.clone'),
        padding: 15
    });
    new joint.ui.Tooltip({
        className: 'tooltip small',
        target: halo.$('.unlink'),
        content: 'Click to break all connections to other objects',
        direction: 'right',
        right: halo.$('.unlink'),
        padding: 15
    });
    new joint.ui.Tooltip({
        className: 'tooltip small',
        target: halo.$('.link'),
        content: 'Click and drag to connect the object',
        direction: 'left',
        left: halo.$('.link'),
        padding: 15
    });
    new joint.ui.Tooltip({
        className: 'tooltip small',
        target: halo.$('.rotate'),
        content: 'Click and drag to rotate the object',
        direction: 'right',
        right: halo.$('.rotate'),
        padding: 15
    });
};

GraphTool.prototype.initializeClipboard= function() {

    this.clipboard = new joint.ui.Clipboard;
    
    KeyboardJS.on('ctrl + c', _.bind(function() {
        // Copy all selected elements and their associated links.
        this.clipboard.copyElements(this.selection, this.graph, { translate: { dx: 20, dy: 20 }, useLocalStorage: true });
    }, this));
    
    KeyboardJS.on('ctrl + v', _.bind(function() {
        this.clipboard.pasteCells(this.graph);
        this.selectionView.cancelSelection();

        this.clipboard.pasteCells(this.graph, { link: { z: -1 }, useLocalStorage: true });

        // Make sure pasted elements get selected immediately. This makes the UX better as
        // the user can immediately manipulate the pasted elements.
        var selectionTmp = [];
        
        this.clipboard.each(function(cell) {

            if (cell.get('type') === 'link') return;

            // Push to the selection not to the model from the clipboard but put the model into the graph.
            // Note that they are different models. There is no views associated with the models
            // in clipboard.
            selectionTmp.push(this.graph.getCell(cell.id));
            this.selectionView.createSelectionBox(this.paper.findViewByModel(cell));
        }, this);

        this.selection.reset(selectionTmp);
    }, this));

    KeyboardJS.on('ctrl + x', _.bind(function() {

        var originalCells = this.clipboard.copyElements(this.selection, this.graph, { useLocalStorage: true });
        this.commandManager.initBatchCommand();
        _.invoke(originalCells, 'remove');
        this.commandManager.storeBatchCommand();
        this.selectionView.cancelSelection();
    }, this));
};

GraphTool.prototype.initializeCommandManager= function() {

    this.commandManager = new joint.dia.CommandManager({ graph: this.graph });

    KeyboardJS.on('ctrl + z', _.bind(function() {
        this.commandManager.undo();
        this.selectionView.cancelSelection();
    }, this));
    
    KeyboardJS.on('ctrl + y', _.bind(function() {
        this.commandManager.redo();
        this.selectionView.cancelSelection();
    }, this));
};

GraphTool.prototype.initializeValidator= function() {

    // This is just for demo purposes. Every application has its own validation rules or no validation
    // rules at all.
    
    this.validator = new joint.dia.Validator({ commandManager: this.commandManager });

   /* this.validator.validate('change:position change:size add', _.bind(function(err, command, next) {
        if (command.action === 'add' && command.batch) return next();

        var cell = command.data.attributes || this.graph.getCell(command.data.id).toJSON();
        var area = g.rect(cell.position.x, cell.position.y, cell.size.width, cell.size.height);

        if (_.find(this.graph.getElements(), function(e) {
	        var position = e.get('position');
                var size = e.get('size');
	        return (e.id !== cell.id && area.intersect(g.rect(position.x, position.y, size.width, size.height)));

        })) return next("Another cell in the way!");
    }, this));*/


    this.validator.on('invalid',function(message) {
        $('.statusbar-container').text(message).addClass('error');
        _.delay(function() {
            $('.statusbar-container').text('').removeClass('error');
        }, 1500);
    });
};

GraphTool.prototype.initializeToolbar= function() {

    this.initializeToolbarTooltips();
    $('#submitElement').on('click',_.bind(this.addTextElement, this));
    //$('#btn-undo').on('click', _.bind(this.commandManager.undo, this.commandManager));
    //$('#btn-redo').on('click', _.bind(this.commandManager.redo, this.commandManager));
    $('#btn-clear').on('click', _.bind(function(){
        rcmModelPathList= new rcmModel.PathList();
        //this.referenceList = [];
        document.getElementsByClassName('references-list')[1].innerHTML='';
        this.graph.clear();
    }, this));

    //dependency with FileSaver
    $('#btn-getfile').on('click', _.bind(function(){
//    	var svgRCM = JSON.stringify(this.graph.toJSON());  
//        console.log("nombre: "+$('#nameRCM').val()+"enviado rcm"+sendRCM);
//        var name = $('#nameRCM').val();
//        sendRCM.svgString= svgRCM;
//        $.post( "/saveGraph", {"name": name ,"jsonString": JSON.stringify(sendRCM)}) ;
        
    	
    	
    	
        var saveRCM = {"pathlist":rcmModelPathList.getPaths(),"nodelist": {"conceptList":rcmRCMNodesList.getConceptList(),"linkingPhraseList":rcmRCMNodesList.getLinkingPhraseList()}}
        var svgRCM = JSON.stringify(this.graph.toJSON());  
        saveRCM.svgString= svgRCM;
        
        var blob = new Blob([JSON.stringify(saveRCM)], {type: "text/plain;charset=utf-8"});
        saveAs(blob, "rcm.rcm");
    }, this));

    $('#btn-importfile').on('click', _.bind(function(){}, this));

    $('#btn-path').on('click', _.bind(this.marcarPath,this));
    $('#btn-png').on('click', _.bind(this.openAsPNG, this));
    //$('#btn-zoom-in').on('click', _.bind(this.zoomIn, this));
    //$('#btn-zoom-out').on('click', _.bind(this.zoomOut, this));
    $('#btn-fullscreen').on('click', _.bind(this.toggleFullscreen, this));
    //$('#btn-print').on('click', _.bind(this.paper.print, this.paper));

    // toFront/toBack must be registered on mousedown. SelectionView empties the selection
    // on document mouseup which happens before the click event. @TODO fix SelectionView?
    //$('#btn-to-front').on('mousedown', _.bind(function(evt) { this.selection.invoke('toFront'); }, this));
    //$('#btn-to-back').on('mousedown', _.bind(function(evt) { this.selection.invoke('toBack'); }, this))

    $('#btn-layout').on('click', _.bind(this.layoutDirectedGraph, this));

    $('#btn-tojson').on('click', _.bind(this.graph2RCM,this));

    $('#btn-definition').on('click', _.bind(this.getDefinition,this));

    $('#input-gridsize').on('change', _.bind(function(evt) {
        var gridSize = parseInt(evt.target.value, 10);
        $('#output-gridsize').text(gridSize);
        this.setGrid(gridSize);
    }, this));
};

GraphTool.prototype.initializeToolbarTooltips= function() {
    
    $('.toolbar-container [data-tooltip]').each(function() {
        
        new joint.ui.Tooltip({
            target: $(this),
            content: $(this).data('tooltip'),
            top: '.toolbar-container',
            direction: 'top'
        });
    });
};

GraphTool.prototype.initializeChannel= function(url) {
    // Example usage of the Channel plugin. Note that this assumes the `node channelHub` is running.
    // See the channelHub.js file for furhter instructions.
    var room = (location.hash && location.hash.substr(1));
    if (!room) {
      room = joint.util.uuid();
      this.navigate('#' + room);
    }

    var channel = this.channel = new joint.com.Channel({ graph: this.graph, url: url || 'ws://localhost:4141', query: { room: room } });
    console.log('room', room, 'channel', channel.id);

    var roomUrl = location.href.replace(location.hash, '') + '#' + room;
    $('.statusbar-container .rt-colab').html('Send this link to a friend to <b>collaborate in real-time</b>: <a href="' + roomUrl + '" target="_blank">' + roomUrl + '</a>');
};

GraphTool.prototype.getRCMList= function(){
    $.ajax("/getRCMNamesList", 
        {   "type": "post",   // usualmente post o get
            "success": function(result) {
                var rcmList = result.split(",");
                var htmlRCMList = '';
                for (var i = 0; i < rcmList.length; i++) {
                    htmlRCMList=htmlRCMList+'<a href="javascript:void(0)"><div onclick="getRCM(this)" class="rcm">'+rcmList[i]+'</div></a>'
                }
                $('#rcmList-rcms').html(htmlRCMList);
            },
            "error": function(result) {
                        console.error("Se ha producido un error: ", result);
            },
            "async": true,
        });
};


























