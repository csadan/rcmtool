var RCMAnalizer = function() {
	
};
RCMAnalizer.prototype = {
	normalizeTex : function(txt) {
		var data = nlp.pos(txt).sentences[0];
		return data.to_present().text();
	},
	deleteDT : function(txt) {
		var data = nlp.pos(txt).sentences[0];
		var newtext = '';
		var taggedwords = data.tokens.map(function(p) {// p.text p.pos.tag
			if (p.pos.tag !== 'DT') {
				newtext = newtext + ' ' + p.normalised;
			} else {
				// console.log("delete: "+p.text)
			}
		})
		return newtext;
	},
	getConcepts_base : function(text) {
		text = this.deleteDT(text);
		var data = nlp.pos(text).sentences[0].tokens;
		var partialResult = [];
		for (var i = 0; i < data.length; i++) {
			if (data[i].pos.parent === "noun") {
				partialResult.push(data[i])
			} else if (data[i].pos.parent === "adjective") {
				partialResult.push(data[i])
			} else if (data[i].pos.tag === "VBG") {
				partialResult.push(data[i])
			}
		}
		;
		var result = [];
		for (var i = 0; i < partialResult.length; i++) {
			// if (partialResult[i].pos.parent==="noun" ||
			// partialResult[i].pos.parent==="adjective") {
			// if (result.length!== 0 &&
			// result[result.length-1].indexOf(partialResult[i].analysis.last.text)!=-1)
			// {
			// result[result.length-1] = result[result.length-1]+'
			// '+partialResult[i].text
			//
			// result.push(partialResult[i].text)
			// }else{
			// result.push(partialResult[i].analysis.last.text+'
			// '+partialResult[i].text)
			// }
			// };
			if (partialResult[i].pos.parent === "noun") {
				if (!partialResult[i].analysis.last) {
					result.push(partialResult[i].text)
				} else {
					// adjective + noun
					if (partialResult[i].analysis.last.pos.parent === "adjective") {
						// evitar
						// ADJ + noun1 + noun2 --> adj noun1 + noun1 noun2
						// de esta forma se comprueba si estaba ya en el
						// anterior resultado, entonces se combinan todos
						if (result.length !== 0
								&& result[result.length - 1]
										.indexOf(partialResult[i].analysis.last.text) != -1) {
							result[result.length - 1] = result[result.length - 1]
									+ ' ' + partialResult[i].text
						} else {
							result.push(partialResult[i].analysis.last.text
									+ ' ' + partialResult[i].text)
						}
					}
					// verb-ing + noun
					else if (partialResult[i].analysis.last.pos.tag === "VBG") {
						if (result.length !== 0
								&& result[result.length - 1]
										.indexOf(partialResult[i].analysis.last.text) != -1) {
							result[result.length - 1] = result[result.length - 1]
									+ ' ' + partialResult[i].text
						} else {
							result.push(partialResult[i].analysis.last.text
									+ ' ' + partialResult[i].text)
						}
					}
					// noun + noun
					else if (partialResult[i].analysis.last.pos.parent === "noun") {
						if (result.length !== 0
								&& result[result.length - 1]
										.indexOf(partialResult[i].analysis.last.text) != -1) {
							result[result.length - 1] = result[result.length - 1]
									+ ' ' + partialResult[i].text
						} else {
							result.push(partialResult[i].analysis.last.text
									+ ' ' + partialResult[i].text)
						}
					}
				}
			}
		}
		;
		console.log("Conceptos [" + result.length + "]: " + result);
		return result;
	},
	getConcepts : function(text) {
		var data = nlp.spot(text)
		var result = [];
		var html = data.map(function(p) {
			// console.log(p.normalised)
			result.push(p.normalised)
		});
		return result;
	},
	getAllElements : function(txt) {
		var data = nlp.pos(txt).sentences[0];
		var result = [];
		data.tokens.map(function(p) {// p.text p.pos.tag
			result.push(p.normalised)
		})
		return result;
	},
	isConcept : function(conceptArray, text) {
		for (var j = 0; j < conceptArray.length; j++) {
			if (text === conceptArray[j]) {
				return true;
			}
		}
		return false;
	},
	pos : function(text) {
		// alsfñdjk
		return nlp.pos(text)
	},
	hasVerb : function(text) {
		var data = nlp.pos(text);
		for (var i = 0; i < data[0].tokens.length; i++) {
			if (data[0].tokens[i].pos.parent === 'verb')
				return true
		}
		;
		return false;
	},
	split : function(text) {
		var conceptsArray = this.getConcepts(text);
		var completTextArray = this.getAllElements(this.deleteDT(text));
		console.log("length: " + completTextArray.length + " ->"
				+ completTextArray);
		var targetConcept = conceptsArray[0];
		var syntagmList = [];
		var syntagm = {};

		for (var i = 1; i < completTextArray.length; i++) {
			if (this.isConcept(conceptsArray, completTextArray[i])) {
				// console.log("concepto: "+completTextArray[i])
				syntagm.concept = completTextArray[i];
			} else {
				var isConcept = false;
				syntagm.linkingphrase = '';
				while (i < completTextArray.length && !isConcept) {
					syntagm.linkingphrase = syntagm.linkingphrase + ' '
							+ completTextArray[i];
					// console.log('linkingphrase: '+syntagm.linkingphrase)

					if (i + 1 < completTextArray.length) {
						isConcept = this.isConcept(conceptsArray,
								completTextArray[i + 1])
						if (!isConcept) {
							i++;
						}
					} else {
						isConcept = true;
						if (this.isConcept(conceptsArray,
								completTextArray[i - 1])) {
							syntagmList[syntagmList.length - 1].concept = syntagmList[syntagmList.length - 1].concept
									+ ' ' + completTextArray[i]
						} else {
							syntagm.linkingphrase = syntagm.linkingphrase
									.replace(completTextArray[i], '')
							syntagm.concept = completTextArray[i]
						}
					}
				}
				// console.log("linkingphrase: linkingphrase);
			}
			if (syntagm.linkingphrase && syntagm.concept) {
				// console.log("syntagma: "+syntagm.linkingphrase+' :: '
				// +syntagm.concept);
				syntagmList.push({
					linkingphrase : syntagm.linkingphrase,
					concept : syntagm.concept
				});
				syntagm.linkingphrase = '';
				syntagm.concept = '';
			}
		}
		console.log(syntagmList)
		return {
			targetConcept : targetConcept,
			syntagmList : syntagmList
		};
	},
	split_improved : function(text) {
		var conceptsArray = this.getConcepts(text)
		var completTextArray = this.deleteDT(text);

		var data = nlp.pos(text).sentences[0];
		var taggedwords = data.tokens.map(function(p) {// p.text p.pos.tag
			if (true) {

			} else if (p.pos.tag !== 'DT') {
				newtext.push(p.normalised);
			} else {
				// console.log("delete: "+p.text)
			}
		})

		console.log(completTextArray);
		var targetConcept = conceptsArray[0];
		var syntagmList = [];
		var syntagm = {};

		for (var i = 1; i < completTextArray.length; i++) {
			if (this.isConcept(conceptsArray, completTextArray[i])) {
				// console.log("concepto: "+completTextArray[i])
				syntagm.concept = completTextArray[i];
			} else {
				var isConcept = false;
				syntagm.linkingphrase = '';
				while (i < completTextArray.length && !isConcept) {
					syntagm.linkingphrase = syntagm.linkingphrase + ' '
							+ completTextArray[i];
					// console.log('linkingphrase: '+syntagm.linkingphrase)

					if (i + 1 < completTextArray.length) {
						isConcept = this.isConcept(conceptsArray,
								completTextArray[i + 1])
						if (!isConcept) {
							i++;
						}
					} else {
						isConcept = true;
						if (this.isConcept(conceptsArray,
								completTextArray[i - 1])) {
							syntagmList[syntagmList.length - 1].concept = syntagmList[syntagmList.length - 1].concept
									+ ' ' + completTextArray[i]
						} else {
							syntagm.linkingphrase = syntagm.linkingphrase
									.replace(completTextArray[i], '')
							syntagm.concept = completTextArray[i]
						}
					}
				}
				// console.log("linkingphrase: linkingphrase);
			}
			if (syntagm.linkingphrase && syntagm.concept) {
				// console.log("syntagma: "+syntagm.linkingphrase+' :: '
				// +syntagm.concept);
				syntagmList.push({
					linkingphrase : syntagm.linkingphrase,
					concept : syntagm.concept
				});
				syntagm.linkingphrase = '';
				syntagm.concept = '';
			}
		}

		syntagmList.push({
			linkingphrase : syntagm.linkingphrase,
			concept : syntagm.concept
		});
		return {
			targetConcept : targetConcept,
			syntagmList : syntagmList
		};
	}
};