  
//DRAG AND DROP REFERENCES
function handleFileSelect(evt) {
    evt.stopPropagation();
    evt.preventDefault();

    var reader = new FileReader();
    reader.onload = function(e) {
      $('#newReferences').val(reader.result);
      console.log(reader.result)
    }

    var files = evt.dataTransfer.files; // FileList object.
    // files is a FileList of File objects. List some properties.
    for (var i = 0, f; f = files[i]; i++) {
      reader.readAsText(f, 'UTF-8');
    }
}

function handleDragOver(evt) {
  evt.stopPropagation();
  evt.preventDefault();
  evt.dataTransfer.dropEffect = 'copy'; // Explicitly show this is a copy.
}

  // Setup the dnd listeners.
var dropZone = document.getElementById('dropzone');
dropZone.addEventListener('dragover', handleDragOver, false);
dropZone.addEventListener('drop', handleFileSelect, false);


//SELECT RCM FILE
function handleFileSelectImport(evt) {

  var reader = new FileReader();
  reader.onload = function(e) {
      app.addRCM(reader.result)
  }

  var files = evt.target.files;
  // files is a FileList of File objects. List some properties.
  for (var i = 0, f; f = files[i]; i++) {
    reader.readAsText(f, 'UTF-8');
  }}
document.getElementById('inputRCM').addEventListener('change', handleFileSelectImport, false);


//Import .bib file
function handleFileBibSelectImport(evt) {

  var reader = new FileReader();
  reader.onload = function(e) {
      addNewReferences(reader.result)
      window.location.href='#';
  }

  var files = evt.target.files;
  // files is a FileList of File objects. List some properties.
  for (var i = 0, f; f = files[i]; i++) {
    reader.readAsText(f, 'UTF-8');
  }}
document.getElementById('inputBib').addEventListener('change', handleFileBibSelectImport, false);