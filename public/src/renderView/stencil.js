
//######################## CUSTOM ELEMENTS #####################################
joint.shapes.basic.LinkingPhrases = joint.shapes.basic.Generic.extend({

    markup: '<g class="rotatable"><g class="scalable"><rect/></g><text/></g>',
    
    defaults: joint.util.deepSupplement({
    
        type: 'basic.LinkingPhrases',
        attrs: {
            'rect': { fill: 'white', stroke: 'white', width: 100, height: 60 },
            'text': { 'font-size': 14, text: '', 'ref-x': .5, 'ref-y': .5, ref: 'rect', 'y-alignment': 'middle', 'x-alignment': 'middle', fill: 'black', 'font-family': 'Arial, helvetica, sans-serif' }
        },
    }, joint.shapes.basic.Generic.prototype.defaults)
});

joint.shapes.basic.Concept = joint.shapes.basic.Generic.extend({

    markup: '<g class="rotatable"><g class="scalable"><rect/></g><text/></g>',
    
    defaults: joint.util.deepSupplement({
    
        type: 'basic.Concept',
        attrs: {
            'rect': { fill: '#FFFFFF', stroke: 'black', width: 100, height: 20 },
            'text': { 'font-size': 14, text: '', 'ref-x': .5, 'ref-y': .5, ref: 'rect', 'y-alignment': 'middle', 'x-alignment': 'middle', fill: 'black', 'font-family': 'Arial, helvetica, sans-serif' }
        },
    }, joint.shapes.basic.Generic.prototype.defaults)
});

/*
joint.shapes.basic.Concept = joint.shapes.basic.Generic.extend({

    markup: '<g class="scalable"><g class="scalable"><rect/></g><text/></g>',
    
    defaults: joint.util.deepSupplement({
        type: 'basic.Concept',
        attrs: {
            'rect': { fill: 'black', stroke: 'black', 'follow-scale': true, width: 80, height: 40 },
            'text': { text:'concept','font-size': 14, 'ref-x': .5, 'ref-y': .5, ref: 'rect', 'y-alignment': 'middle', 'x-alignment': 'middle' }
        },
        path: []
    }, joint.shapes.basic.Generic.prototype.defaults)
});
*/


var Stencil = {};


Stencil.groups = {
    basic: { index: 1, label: 'RCM elements' },
};


//########################### ELEMENTS IN STENCIL ###############################
Stencil.shapes = {
    basic: [
        new joint.shapes.basic.Concept({
            size: { width: 10, height: 10},
            attrs:  {
                    rect: {rx: 2, ry: 2, width:25, height: 20,fill: '#045FB4'},
                     text: { text: 'concept', fill: 'black', 'font-size': 11, 'stroke-width': 0.5 }
                    },
            path: []
        }),
        new joint.shapes.basic.LinkingPhrases({
            size: { width: 10, height: 10},
            attrs:  {
                    rect: {rx: 2, ry: 2, width:25, height: 10,fill: 'white'},
                     text: {'font-size': 11, text: 'LinkingPhrase', fill: 'black', stroke: 'black', 'stroke-width': 0}
                    },
            path: []
        }),
    ]
};
