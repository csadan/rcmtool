/* global MendeleySDK */
/* jshint camelcase: false */


var OAuthManage = function(){};
OAuthManage.prototype={
		"renderDocumentList": function (data) {
		    var reflist ='';
		    for (var i = 0; i < data.length; i++) {
		        if (!data[i].authors) {
		            data[i].authors=[{"first_name":"No authors", "last_name":""}];
		        }
		        app.referenceList.push(new rcmModel.Reference(i+1,data[i].title,data[i].type,data[i].authors,data[i].year,data[i].abstract))
		        reflist=reflist + '<a href="javascript:void(0)"><div class="reference" id="'+(i+1)+'"onclick="addRef(this)"><div class="colLeft"><div class="idRef">['+(i+1)+']</div><div class="yearRef">'+data[i].year+'</div></div><div class="colRight"><div class="titleRef">'+data[i].title+'</div><div class="authRef">'+data[i].authors[0].first_name+' '+data[i].authors[0].last_name+'</div></div></div></a>'  
		    };
		    $("#refs").html( reflist);

		    // añado los tooltip para que muestre los abstracts que queda molon
		    for (var i = 1; i <app.referenceList.length; i++) {
		        new joint.ui.Tooltip({
		            target: '#'+i,
		            content: (typeof app.referenceList[i].getAbstract()==='undefined')?'No abstract':app.referenceList[i].getAbstract(),
		            left: '.references-container',
		            direction: 'left'
		        });
		    };
		},
		"getDocuments": function (event) {
		    MendeleySDK.API.documents
		        .list({ sort: 'created', order: 'desc', limit:'500' })
		        .done(this.renderDocumentList)
		        .fail(errorHandler);
		}
}








//TODO:
//// no van los siguientes métodos
//var renderFoldersList = function (folders) {
//    var i, j, len, jlen, folder, $folder,
//        $list = $('.folders').find('.list').empty().removeClass('populated'),
//        documentTemplate = $('#foldersTemplate').html(),
//        authorTemplate = $('#authorElement').html();
//
//    for (i = 0, len = folders.length, folder; i < len; i+=1) {
//        folder = folders[i];
//        $folder = $(documentTemplate);
//        $folder.attr('data-pos', i+1);
//        $folder.attr('data-id', folder.id);
//        $folder.find('.title').html(folder.name);
//        $folder.find('.added').html((new Date(folder.created)).toLocaleString());
//        $list.append($folder);
//    }
//    $list.addClass('populated');
//};
//
//var renderFileList = function (files) {
//    var i, len, file, $file, 
//        $list = $('.files').find('.list').empty().removeClass('populated'),
//        fileTemplate = $('#fileTemplate').html();
//
//    for (i = 0, len = files.length, file; i < len; i+=1) {
//        file = files[i];
//        $file = $(fileTemplate);
//        $file.attr('data-pos', i+1);
//        $file.attr('data-id', file.id);
//        $file.find('.title').html(file.file_name);
//        $file.find('.size').html('Size: ' + file.size);
//        $list.append($file);
//    }
//    $list.addClass('populated');
//};
//
//var renderDocument = function (doc) {
//    var $doc,
//        $list = $('.documents').find('.create'),
//        documentTemplate = $('#documentTemplate').html();
//
//    if ($list.find('.empty').length) {
//        $list.empty();
//    }
//    $doc = $(documentTemplate);
//    $doc.removeAttr('data-pos');
//    $doc.attr('data-id', doc.id);
//    $doc.find('.title').html(doc.title);
//    $doc.find('.added').html((new Date(doc.created)).toLocaleString());
//    $list.append($doc);
//    $list.addClass('populated');
//};
//
//var errorHandler = function (req, res) {
//    var response;
//
//    console.error('Request failed with status code:', res.status);
//    if (res && res.responseText) {
//        response = JSON.parse(res.responseText);
//        if (response.hasOwnProperty('message')) {
//            console.error('Response error message:', response.message);
//        } else {
//            console.info('No response error message received');
//        }
//    } else {
//        console.info('No response body received');
//    }
//};
//
//
//
//var getFolders = function (event) {
//    MendeleySDK.API.folders
//        .list()
//        .done(renderFoldersList)
//        .fail(errorHandler);
//    event.preventDefault();
//};
//
//var createDocument = function (event) {
//    MendeleySDK.API.documents
//        .create({
//            title: $('.document-title').val() || 'A new document',
//            type: $('.document-type').val() || 'journal'
//        })
//        .done(renderDocument)
//        .fail(errorHandler);
//    event.preventDefault();
//};
//
//var getFiles = function (event) {
//    MendeleySDK.API.files
//        .list($('.file-id').val())
//        .done(renderFileList)
//        .fail(errorHandler);
//    event.preventDefault();
//};
//
//var clearDocuments = function (event) {
//    var $list = $('.documents').find('.create'),
//        listItemTemplate = $('#listItemTemplate').html();
//
//    if ($list.hasClass('populated')) {
//        $list.empty().removeClass('populated').append(listItemTemplate);
//    }
//    event.preventDefault();
//};
