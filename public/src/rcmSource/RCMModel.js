rcmModel = (function() {
	var abecedario = [ 'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k',
			'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x',
			'y', 'z' ];

	/*
	 * #################################### CLASS REFERENCE
	 * #############################################################
	 */
	var Reference = function(id, title, type, authors, year, abstract) { // todo
																			// strings
		this.id = id;
		this.title = title;
		this.type = type;
		this.authors = authors;
		this.year = year;
		this.abstract = abstract;
	};
	Reference.prototype = {
		getID : function() {
			return this.id
		},
		setID : function(id) {
			this.id = id
		},
		getTitle : function() {
			return this.title
		},
		getType : function() {
			return this.type
		},
		getAuthors : function() {
			return this.authors
		},
		getYear : function() {
			return this.year
		},
		getAbstract : function() {
			return this.abstract
		}
	}
	/*
	 * ##################################### CLASS SYNTAGM
	 * ##########################################################
	 */
	var Syntagm = function(linkingPhrase, concept) { // paso los
														// identificadores
		this.linkingPhrase = linkingPhrase; // id de lp
		this.concept = concept; // id de concept
	};
	Syntagm.prototype = {
		setLinkingPhrase : function(linkingPhrase) {
			this.linkingPhrase = linkingPhrase
		},
		getLinkingPhrase : function() {
			return this.linkingPhrase
		},
		setConcept : function(concept) {
			this.concept = concept
		},
		getConcept : function() {
			return this.concept
		},
	};
	/*
	 * ######################################## CLASS PATH
	 * ####################################################
	 */
	var Path = function(id, suffix, reference, firstConcept) { // todos strings
																// excepto el
																// último
																// (firstConcept)
																// que es de
																// tipo CONCEPT
		this.id = id; // 1,2,3,4,5.....
		this.suffix = suffix; // abecedario
		this.reference = reference;
		this.firstConcept = firstConcept;
		this.syntagmList = []; // lista de identificadores de syntagmas
		// el primer sintama está compuesto únicamente por un concepto
	};
	Path.prototype = {
		getFirstConcept : function() {
			return this.firstConcept
		},
		setID : function(id) {
			this.id = id
		},
		getID : function() {
			return this.id
		},
		getAllID : function() {
			return this.id + this.suffix;
		},
		setSuffix : function(suffix) {
			this.suffix = suffix
		},
		getSuffix : function() {
			return this.suffix
		},
		getReference : function() {
			return this.reference;
		},
		getSyntagms : function() {
			return this.syntagmList;
		},
		addSyntagm : function(syntagm) { // identificadores
			for (var i = 0; i < this.syntagmList.length; i++) { // comprobar que
																// no esté
				if (this.syntagmList[i].getConcept() === syntagm.getConcept()
						&& this.syntagmList[i].getLinkingPhrase() === syntagm
								.getLinkingPhrase())
					return false
			}
			this.syntagmList.push(syntagm);
			return true;
		},
		deleteSyntagm : function(syntagm) {
			for (var i = 0; i < this.syntagmList.length; i++) {
				if (this.syntagmList[i].getConcept() === syntagm.getConcept()
						&& this.syntagmList[i].getLinkingPhrase() === syntagm
								.getLinkingPhrase()) {
					this.syntagmList.splice(i, 1);
					return true;
				}
			}
			return false;
		},
		numberLinkingPhrases : function(linkingphrase) {
			rtd = 0;
			for (var i = 0; i < this.syntagmList.length; i++) {
				if (this.syntagmList[i].getLinkingPhrase() === linkingphrase) {
					rtd++;
				}
				;
			}
			return rtd;
		}
	};
	/*
	 * ######################################## CLASS RCMNodes
	 * ####################################################
	 */
	var RCMNodesList = function() {
		this.conceptList = {};
		this.linkingPhraseList = {};
	};
	RCMNodesList.prototype = {
		getConceptList : function() {
			return this.conceptList;
		},
		getConcept : function(idConcept) {
			// return
			return this.conceptList[idConcept];
		},
		hasConcept : function(idConcept) {
			// return
			return this.conceptList[idConcept] ? true : false;
		},
		getConceptByText : function(textConcept) {
			for ( var i in this.conceptList) {
				if (this.conceptList[i].text === textConcept)
					return i;
			}
			return null;
		},
		hasConceptByText : function(textConcept) {
			for ( var i in this.conceptList) {
				if (this.conceptList[i].text === textConcept)
					return true;
			}
			return false;
		},
		setConcept : function(concept) {
			// return
			this.conceptList[concept.id] = {
				"id" : concept.id,
				"text" : concept.attr("text/text")
			};
		},
		addConcept : function(concept) {
			if (this.conceptList[concept.id]) {
				return false
			} else {
				for ( var i in this.conceptList) {
					if (this.conceptList[i].text === concept.text)
						return false;
				}
				this.conceptList[concept.id] = {
					"id" : concept.id,
					"text" : concept.text
				};
				return true;
			}
		},
		deleteConcept : function(concept) {
			if (this.conceptList[concept.id]) {
				this.conceptList[concept.id] = null;
				return true;
			} else {
				return false;
			}
		},
		getLinkingPhraseList : function() {
			// return
			return this.linkingPhraseList;
		},
		getLinkingPhrase : function(idLinkingphrase) {
			// return
			return this.linkingPhraseList[idLinkingphrase];
		},
		getLinkingPhraseByText : function(textLinkingPhrase) {
			var rtd = []
			for ( var i in this.linkingPhraseList) {
				if (this.linkingPhraseList[i].text === textLinkingPhrase)
					rtd.push(i);
			}
			return rtd;
		},
		setLinkingPhrase : function(linkingphrase) {
			// return
			this.linkingPhraseList[linkingphrase.id] = {
				"id" : linkingphrase.id,
				"text" : linkingphrase.attr("text/text")
			};
		},
		hasLinkingPhrase : function(idLinkingphrase) {
			// return
			return this.linkingPhraseList[idLinkingphrase] ? true : false;
		},
		addLinkingPhrase : function(linkingphrase) {
			if (this.linkingPhraseList[linkingphrase.id]) {
				return false
			} else {
				var analizer = new RCMAnalizer();
				if (analizer.pos(linkingphrase.text).sentences[0].tokens[0].pos.parent === 'verb') { // en
																										// caso
																										// de
																										// que
																										// empiece
																										// por
																										// verbo
																										// no
																										// se
																										// repetirá
					for ( var i in this.linkingPhraseList) {
						if (this.linkingPhraseList[i].text === linkingphrase.text)
							return false;
					}
				}
				this.linkingPhraseList[linkingphrase.id] = {
					"id" : linkingphrase.id,
					"text" : linkingphrase.text
				};
				return true;
			}
		},
		deleteLinkingPhrase : function(linkingphrase) {
			if (this.linkingPhraseList[linkingphrase.attr("text/text")]) {
				this.linkingPhraseList[linkingphrase.attr("text/text")] = null;
				return true;
			} else {
				return false;
			}
		},
	}
	/*
	 * ##################################### CLASS PATHLIST
	 * ########################################################
	 */
	var PathList = function() {
		// asfdjl
		this.paths = [];
	};
	PathList.prototype = {
		hasSyntagm : function(idLPList, idConcept) {
			for (var i = 0; i < this.paths.length; i++) {
				for (var j = 0; j < this.paths[i].getSyntagms().length; j++) {
					for (var k = 0; k < idLPList.length; k++) {
						if (idLPList[k] === this.paths[i].getSyntagms()[j]
								.getLinkingPhrase()
								&& idConcept == this.paths[i].getSyntagms()[j]
										.getConcept()) {
							return true;
						}
					}
				}
			}
			return false;
		},
		getPaths : function() {
			return this.paths
		},
		hasPath : function(path) {
			for (var i = 0; i < this.paths.length; i++) {
				if (this.paths[i].getID() === path)
					return true;
			}
			;
			return false;
		},
		getPathsById : function(path) {
			var res = [];
			for (var i = 0; i < this.paths.length; i++) {
				if (path == this.paths[i].getID()) {
					res.push(this.paths[i]);
				}
				;
			}
			;
			return res;
		},
		getPathsByIdAndSuffix : function(path, suffix) {
			var res = [];
			for (var i = 0; i < this.paths.length; i++) {
				if (this.paths[i].getID() == path
						&& this.paths[i].getSuffix() == suffix)
					return this.paths[i];
			}
			;
			return null;
		},
		addPath : function(path) {
			var n = 0;
			for (var i = 0; i < this.paths.length; i++) {
				if (this.paths[i].getID() == path.getID())
					n++;
			}
			path.setSuffix(abecedario[n]);
			this.paths.push(path);
			console.log(this.paths)
			this.paths.sort(function(a, b) {
				if (a.getID() === b.getID())
					return a.getSuffix() - b.getSuffix();
				else
					return a.getID() - b.getID();
			})
		},
		deletePath : function(id) {
			for (var i = 0; i < this.paths.length; i++) {
				var suffix = (typeof this.paths[i].getSuffix() !== 'undefined') ? this.paths[i]
						.getSuffix()
						: '';
				var idAux = this.paths[i].getID() + '' + suffix;
				if (idAux === id) {
					this.paths.splice(i, 1);
					return true;
				}
			}
			return false;
		}
	};
	/*
	 * ####################################### ANALIZADOR
	 * ######################################################
	 */
	var sintaxAnalizer = function() {
	};
	sintaxAnalizer.prototype = {
		normalizeTex : function(txt) {
			var data = nlp.pos(txt).sentences[0];
			return data.to_present().text();
		},
		deleteDT : function(txt) {
			var data = nlp.pos(txt).sentences[0];
			var newtext = '';
			var taggedwords = data.tokens.map(function(p) {// p.text p.pos.tag
				if (p.pos.tag !== 'DT') {
					newtext = newtext + ' ' + p.normalised;
				} else {
					// console.log("delete: "+p.text)
				}
			})
			return newtext;
		},
		getConcepts_base : function(text) {
			text = this.deleteDT(text);
			var data = nlp.pos(text).sentences[0].tokens;
			var partialResult = [];
			for (var i = 0; i < data.length; i++) {
				if (data[i].pos.parent === "noun") {
					partialResult.push(data[i])
				} else if (data[i].pos.parent === "adjective") {
					partialResult.push(data[i])
				} else if (data[i].pos.tag === "VBG") {
					partialResult.push(data[i])
				}
			}
			;
			var result = [];
			for (var i = 0; i < partialResult.length; i++) {
				// if (partialResult[i].pos.parent==="noun" ||
				// partialResult[i].pos.parent==="adjective") {
				// if (result.length!== 0 &&
				// result[result.length-1].indexOf(partialResult[i].analysis.last.text)!=-1)
				// {
				// result[result.length-1] = result[result.length-1]+'
				// '+partialResult[i].text
				//
				// result.push(partialResult[i].text)
				// }else{
				// result.push(partialResult[i].analysis.last.text+'
				// '+partialResult[i].text)
				// }
				// };
				if (partialResult[i].pos.parent === "noun") {
					if (!partialResult[i].analysis.last) {
						result.push(partialResult[i].text)
					} else {
						// adjective + noun
						if (partialResult[i].analysis.last.pos.parent === "adjective") {
							// evitar
							// ADJ + noun1 + noun2 --> adj noun1 + noun1 noun2
							// de esta forma se comprueba si estaba ya en el
							// anterior resultado, entonces se combinan todos
							if (result.length !== 0
									&& result[result.length - 1]
											.indexOf(partialResult[i].analysis.last.text) != -1) {
								result[result.length - 1] = result[result.length - 1]
										+ ' ' + partialResult[i].text
							} else {
								result.push(partialResult[i].analysis.last.text
										+ ' ' + partialResult[i].text)
							}
						}
						// verb-ing + noun
						else if (partialResult[i].analysis.last.pos.tag === "VBG") {
							if (result.length !== 0
									&& result[result.length - 1]
											.indexOf(partialResult[i].analysis.last.text) != -1) {
								result[result.length - 1] = result[result.length - 1]
										+ ' ' + partialResult[i].text
							} else {
								result.push(partialResult[i].analysis.last.text
										+ ' ' + partialResult[i].text)
							}
						}
						// noun + noun
						else if (partialResult[i].analysis.last.pos.parent === "noun") {
							if (result.length !== 0
									&& result[result.length - 1]
											.indexOf(partialResult[i].analysis.last.text) != -1) {
								result[result.length - 1] = result[result.length - 1]
										+ ' ' + partialResult[i].text
							} else {
								result.push(partialResult[i].analysis.last.text
										+ ' ' + partialResult[i].text)
							}
						}
					}
				}
			}
			;
			console.log("Conceptos [" + result.length + "]: " + result);
			return result;
		},
		getConcepts : function(text) {
			var data = nlp.spot(text)
			var result = [];
			var html = data.map(function(p) {
				// console.log(p.normalised)
				result.push(p.normalised)
			});
			return result;
		},
		getAllElements : function(txt) {
			var data = nlp.pos(txt).sentences[0];
			var result = [];
			data.tokens.map(function(p) {// p.text p.pos.tag
				result.push(p.normalised)
			})
			return result;
		},
		isConcept : function(conceptArray, text) {
			for (var j = 0; j < conceptArray.length; j++) {
				if (text === conceptArray[j]) {
					return true;
				}
			}
			return false;
		},
		pos : function(text) {
			// alsfñdjk
			return nlp.pos(text)
		},
		hasVerb : function(text) {
			var data = nlp.pos(text);
			for (var i = 0; i < data[0].tokens.length; i++) {
				if (data[0].tokens[i].pos.parent === 'verb')
					return true
			}
			;
			return false;
		},
		split : function(text) {
			var conceptsArray = this.getConcepts(text);
			var completTextArray = this.getAllElements(this.deleteDT(text));
			console.log("length: " + completTextArray.length + " ->"
					+ completTextArray);
			var targetConcept = conceptsArray[0];
			var syntagmList = [];
			var syntagm = {};

			for (var i = 1; i < completTextArray.length; i++) {
				if (this.isConcept(conceptsArray, completTextArray[i])) {
					// console.log("concepto: "+completTextArray[i])
					syntagm.concept = completTextArray[i];
				} else {
					var isConcept = false;
					syntagm.linkingphrase = '';
					while (i < completTextArray.length && !isConcept) {
						syntagm.linkingphrase = syntagm.linkingphrase + ' '
								+ completTextArray[i];
						// console.log('linkingphrase: '+syntagm.linkingphrase)

						if (i + 1 < completTextArray.length) {
							isConcept = this.isConcept(conceptsArray,
									completTextArray[i + 1])
							if (!isConcept) {
								i++;
							}
						} else {
							isConcept = true;
							if (this.isConcept(conceptsArray,
									completTextArray[i - 1])) {
								syntagmList[syntagmList.length - 1].concept = syntagmList[syntagmList.length - 1].concept
										+ ' ' + completTextArray[i]
							} else {
								syntagm.linkingphrase = syntagm.linkingphrase
										.replace(completTextArray[i], '')
								syntagm.concept = completTextArray[i]
							}
						}
					}
					// console.log("linkingphrase: linkingphrase);
				}
				if (syntagm.linkingphrase && syntagm.concept) {
					// console.log("syntagma: "+syntagm.linkingphrase+' :: '
					// +syntagm.concept);
					syntagmList.push({
						linkingphrase : syntagm.linkingphrase,
						concept : syntagm.concept
					});
					syntagm.linkingphrase = '';
					syntagm.concept = '';
				}
			}
			console.log(syntagmList)
			return {
				targetConcept : targetConcept,
				syntagmList : syntagmList
			};
		},
		split_improved : function(text) {
			var conceptsArray = this.getConcepts(text)
			var completTextArray = this.deleteDT(text);

			var data = nlp.pos(text).sentences[0];
			var taggedwords = data.tokens.map(function(p) {// p.text p.pos.tag
				if (true) {

				} else if (p.pos.tag !== 'DT') {
					newtext.push(p.normalised);
				} else {
					// console.log("delete: "+p.text)
				}
			})

			console.log(completTextArray);
			var targetConcept = conceptsArray[0];
			var syntagmList = [];
			var syntagm = {};

			for (var i = 1; i < completTextArray.length; i++) {
				if (this.isConcept(conceptsArray, completTextArray[i])) {
					// console.log("concepto: "+completTextArray[i])
					syntagm.concept = completTextArray[i];
				} else {
					var isConcept = false;
					syntagm.linkingphrase = '';
					while (i < completTextArray.length && !isConcept) {
						syntagm.linkingphrase = syntagm.linkingphrase + ' '
								+ completTextArray[i];
						// console.log('linkingphrase: '+syntagm.linkingphrase)

						if (i + 1 < completTextArray.length) {
							isConcept = this.isConcept(conceptsArray,
									completTextArray[i + 1])
							if (!isConcept) {
								i++;
							}
						} else {
							isConcept = true;
							if (this.isConcept(conceptsArray,
									completTextArray[i - 1])) {
								syntagmList[syntagmList.length - 1].concept = syntagmList[syntagmList.length - 1].concept
										+ ' ' + completTextArray[i]
							} else {
								syntagm.linkingphrase = syntagm.linkingphrase
										.replace(completTextArray[i], '')
								syntagm.concept = completTextArray[i]
							}
						}
					}
					// console.log("linkingphrase: linkingphrase);
				}
				if (syntagm.linkingphrase && syntagm.concept) {
					// console.log("syntagma: "+syntagm.linkingphrase+' :: '
					// +syntagm.concept);
					syntagmList.push({
						linkingphrase : syntagm.linkingphrase,
						concept : syntagm.concept
					});
					syntagm.linkingphrase = '';
					syntagm.concept = '';
				}
			}

			syntagmList.push({
				linkingphrase : syntagm.linkingphrase,
				concept : syntagm.concept
			});
			return {
				targetConcept : targetConcept,
				syntagmList : syntagmList
			};
		}
	};
	return {
		abecedario : abecedario,
		Reference : Reference,
		Path : Path,
		Syntagm : Syntagm,
		PathList : PathList,
		NodeList : RCMNodesList,
		Analizer : sintaxAnalizer,
	};
})();
