/*jshint camelcase: false */
module.exports = function(app, config) {

    'use strict';

    var oauth2 = require('simple-oauth2')({
        site: 'https://api.mendeley.com',
        clientID: config.clientId,
        clientSecret: config.clientSecret,
        tokenPath: '/oauth/token'
    });

    var cookieParser = require('cookie-parser');
    var accessTokenCookieName = 'accessToken';
    var refreshTokenCookieName = 'refreshToken';
    var oauthPath = '/oauth';
    var examplesPath = '/rcmtool';
    var tokenExchangePath = '/oauth/token-exchange';      

    app.use(cookieParser());


    app.get('/oauth', function (req, res) {
        var authorizationUri = oauth2.authCode.authorizeURL({
            redirect_uri: config.redirectUri, //"http://localhost"+':19313'+'/oauth/token-exchange';
            scope: config.scope || 'all'
        });
        console.log("OATUH: redireccionar a "+authorizationUri);
        res.redirect(authorizationUri);
    });

    app.get('/oauth/token-exchange', function (req, res, next) {
        var code = req.query.code;
        console.log('token-exchange: Starting token exchange with code '+code+' and '+config.redirectUri);

        oauth2.authCode.getToken({
            redirect_uri: config.redirectUri,
            code: code,
        }, function(error, result) {
            if (error) {
                console.log('Error exchanging token: '+error);
                res.redirect('/logout')
            } else {
                setCookies(res, result);
                console.log("token-exchange: redireccionar a "+examplesPath);
                res.redirect(examplesPath);
            }
        });
    });


    app.get('/login', function(req, res) {
        console.log('Logging in, clearing any existing cookies');
        res.clearCookie(accessTokenCookieName);
        res.clearCookie(refreshTokenCookieName);
        res.redirect(oauthPath);
    });

    app.get('/oauth/refresh', function(req, res, next) {

        console.log('Attempting to refresh access token');

        var cookies = req.cookies, json = '{ message: "unknown error"}', status;

        res.set('Content-Type', 'application/json');

        // No cookies? Don't bother trying to refresh and send a 401
        if (!cookies[refreshTokenCookieName]) {
            console.log('Cannot refresh as no refresh token cookie available')
            status = 401;
            json = '{ message: "Refresh token unavailable" }';
            res.status(status).send(json);
        }
        // Otherwise attempt refresh
        else {
            oauth2.AccessToken.create({
                access_token: cookies[accessTokenCookieName],
                refresh_token: cookies[refreshTokenCookieName]
            }).refresh(function(error, token) {
                // On error send a 401
                if (error) {
                    status = 401;
                    json = '{ message: "Refresh token invalid" }';
                }
                // Otherwise put new access/refresh token in cookies and send 200
                else {
                    status = 200;
                    setCookies(res, token.token);
                    json = '{ message: "Refresh token succeeded" }';
                }
                console.log('Refresh result:', status, json)
                res.status(status).send(json);
            });
        }
    });

    function setCookies(res, token) {
        res.cookie(accessTokenCookieName, token.access_token, { maxAge: token.expires_in * 1000 });
        res.cookie(refreshTokenCookieName, token.refresh_token, { httpOnly: true });
    }
};
