// Inspector plugin.
// -----------------

// This plugin creates a two-way data-binding between the cell model and a generated
// HTML form with input fields of a type declaratively specified in an options object passed
// into the element inspector.

/*
USAGE:

var inspector = new joint.ui.Inspector({
    cellView: cellView,
    inputs: {
            attrs: {
                text: {
                    'font-size': { type: 'number', min: 5, max: 80, group: 'text', index: 2 },
                    'text': { type: 'textarea', group: 'text', index: 1 }
                }
            },
            position: {
                x: { type: 'number', group: 'geometry', index: 1 },
                y: { type: 'number', group: 'geometry', index: 2 }
            },
            size: {
                width: { type: 'number', min: 1, max: 500, group: 'geometry', index: 3 },
                height: { type: 'number', min: 1, max: 500, group: 'geometry', index: 4 }
            },
            mydata: {
                foo: { type: 'textarea', group: 'data' }
            }
   },
   groups: {
           text: { label: 'Text', index: 1 },
           geometry: { label: 'Geometry', index: 2, closed: true },
           data: { label: 'data', index: 3 }
   }
});

$('.inspector-container').append(inspector.render().el);
*/

joint.ui.Inspector = Backbone.View.extend({

    className: 'inspector',

    options: {
        cellView: undefined,    // One can pass either a cell view ...
        cell: undefined,        // ... or the cell itself.
        live: true      // By default, we enabled live changes from the inspector inputs.
    },

    events: {
        'mousedown': 'startBatchCommand',
        'change': 'onChangeInput',
        'click .group-label': 'onGroupLabelClick',
        'click .btn-list-add': 'addListItem',
        'click .btn-list-del': 'deleteListItem'
    },

    initialize: function() {

        this.options.groups = this.options.groups || {};

        _.bindAll(this, 'stopBatchCommand');

        // Start a batch command on `mousedown` over the inspector and stop it when the mouse is
        // released anywhere in the document. This prevents setting attributes in tiny steps
        // when e.g. a value is being adjusted through a slider. This gives other parts
        // of the application a chance to treat the serveral little changes as one change.
        // Consider e.g. the CommandManager plugin.
        $(document).on('mouseup', this.stopBatchCommand);
        
        
        // Flatten the `inputs` object until the level where the options object is.
        // This produces an object with this structure: { <path>: <options> }, e.g. { 'attrs/rect/fill': { type: 'color' } }
        this.flatAttributes = joint.util.flattenObject(this.options.inputs, '/', function(obj) {
            // Stop flattening when we reach an object that contains the `type` property. We assume
            // that this is our options object. @TODO This is not very robust as there could
            // possibly be another object with a property `type`. Instead, we should stop
            // just before the nested leaf object.
            return obj.type;
        });

        // `_when` object maps path to a set of conditions (either `eq` or `regex`).
        // When an input under the path changes to
        // the value that equals all the `eq` values or matches all the `regex` regular expressions,
        // the inspector rerenders itself and this time includes all the
        // inputs that met the conditions.
        this._when = {};

        // `_bound` object maps a slave path to a master path (A slave is using master's data).
        // When an input under the master path changes, the inspecor rerenders the input under the
        // slave path
        this._bound = {};

        // Add the attributes path the options object as we're converting the flat object to an array
        // and so we would loose the keys otherwise.
        var attributesArray = _.map(this.flatAttributes, function(options, path) {

            if (options.when && options.when.eq) {
                _.each(options.when.eq, function(condValue, condPath) {
                    var dependant = { value: condValue, path: path };
                    (this._when[condPath] || (this._when[condPath] = [])).push(dependant);
                }, this);
            }

            if (options.when && options.when.regex) {
                _.each(options.when.regex, function(condRegex, condPath) {
                    var dependant = { regex: condRegex, path: path };
                    (this._when[condPath] || (this._when[condPath] = [])).push(dependant);
                }, this);
            }

            // If the option type is 'select' and its options needs resolving (is defined by path)
            // we bind the select (slave) and the input under the path (master) together.
            if (options.type == 'select' && _.isString(options.options)) {
                // slave : master
                this._bound[path] = options.options;
            }

            options.path = path;
            return options;

        }, this);

        // Sort the flat attributes object by two criteria: group first, then index inside that group.
        this.groupedFlatAttributes = _.sortBy(attributesArray, function(options, path) {

            var groupOptions = this.options.groups[options.group];
            return groupOptions ? 'a' + groupOptions.index + 'b' + options.index : Number.MAX_VALUE;
                
        }, this);

        // Cache all the attributes (inputs, lists and objects) with every change to the DOM tree.
        // Chache it by its path.
        this.on('render', function() {

            this._byPath = {};
            
            _.each(this.$('[data-attribute]'), function(attribute) {
                var $attribute = $(attribute);
                this._byPath[$attribute.attr('data-attribute')] = $attribute;
            }, this);
            
        }, this);

        // Listen on events on the cell.
        this.listenTo(this.getModel(), 'all', this.onCellChange, this);
    },

    getModel: function() {
        return this.options.cell || this.options.cellView.model;
    },

    onCellChange: function(eventName, cell, change, opt) {

        opt = opt || {};

        // Do not react on changes that happened inside this inspector. This would
        // cause a re-render of the same inspector triggered by an input change in this inspector.
        if (opt['inspector_' + this.cid]) return;

        // Note that special care is taken for all the transformation attribute changes
        // (`position`, `size` and `angle`). See below for details.
        
        switch (eventName) {
            
          case 'remove':
            // Make sure the element inspector gets removed when the cell is removed from the graph.
            // Otherwise, a zombie cell could possibly be updated.
            this.remove();
            break;
          case 'change:position':
            // Make a special case for `position` as this one is performance critical.
            // There is no need to rerender the whole inspector but only update the position input.
            this.updateInputPosition();
            break;
          case 'change:size':
            // Make a special case also for the `size` attribute for the same reasons as for `position`.
            this.updateInputSize();
            break;
          case 'change:angle':
            // Make a special case also for the `angle` attribute for the same reasons as for `position`.
            this.updateInputAngle();
            break;
          case 'change:source':
          case 'change:target':
            // Make a special case also for the 'source' and 'target' of a link for the same reasons
            // as for 'position'. We don't expect source or target to be configurable.
            // That's why we do nothing here.
            break;
        default:
            // Re-render only on specific attributes changes. These are all events that starts with `'change:'`.
            // Otherwise, the re-render would be called unnecessarily (consider generic `'change'` event, `'bach:start'`, ...).
            var changeAttributeEvent = 'change:';
            if (eventName.slice(0, changeAttributeEvent.length) === changeAttributeEvent) {
                
                this.render();
            }
            break;
        }
    },

    render: function() {

        this.$el.empty();

        var lastGroup;
        var $groups = [];
        var $group;
        
        _.each(this.groupedFlatAttributes, function(options) {

            if (lastGroup !== options.group) {
                // A new group should be created.

                var groupOptions = this.options.groups[options.group];
                var groupLabel = groupOptions ? groupOptions.label || options.group : options.group;
                
                $group = $(joint.templates.inspector['group.html']({ label: groupLabel }));
                $group.attr('data-name', options.group);
                if (groupOptions && groupOptions.closed) $group.addClass('closed');
                $groups.push($group);
            }
            
            this.renderTemplate($group, options, options.path);

            lastGroup = options.group;
            
        }, this);

        this.$el.append($groups);

        this.trigger('render');
        
        return this;
    },

    // Get the value of the attribute at `path` based on the `options.defaultValue`,
    // and `options.valueRegExp` if present.
    getCellAttributeValue: function(path, options) {

        var cell = this.getModel();

        var value = joint.util.getByPath(cell.attributes, path, '/');
        if (!options) return value;

        if (_.isUndefined(value) && !_.isUndefined(options.defaultValue)) {
            value = options.defaultValue;
        }

        if (options.valueRegExp) {

            if (_.isUndefined(value)) {
                
                throw new Error('Inspector: defaultValue must be present when valueRegExp is used.');
            }
            
            var valueMatch = value.match(new RegExp(options.valueRegExp));
            value = valueMatch && valueMatch[2];
        }

        return value;
    },

    // Check conditionals and return `true` if any of them apply. Otherwise, return `false`.
    // If conditionals apply for such an `options` object, we want to hide the enclosing `.field`.
    guard: function(options) {

        var valid = true;

        if (options.when && options.when.eq) {

            var notValid = _.find(options.when.eq, function(condValue, condPath) {
                var value = this.getCellAttributeValue(condPath);
                // If value of the `eq` clause is not identical to the value of the cell attribute under
                // the `eq` clause path, the input didn't meet the conditions and so we don't want to display it.
                if (value !== condValue) return true;
                return false;
            }, this);
            
            if (notValid) valid = false;
        }

        if (options.when && options.when.regex) {

            var notValid = _.find(options.when.regex, function(condRegex, condPath) {
                var value = this.getCellAttributeValue(condPath);
                // If the `regex` clause does not match the value of the cell attribute under
                // the `regex` clause path, the input didn't meet the conditions and so we don't want to display it.
                
                if (!(new RegExp(condRegex)).test(value)) return true;
                return false;
            }, this);
            
            if (notValid) valid = false;
        }

        return !valid;
    },

    resolveBindings: function(options) {

        switch(options.type) {

          case 'select':

            var items = options.options;

            if (_.isString(items)) {

                options.items = joint.util.getByPath(this.getModel().attributes, items, '/');

            } else {

                options.items = items;
            }

            break;
        }
    },

    updateBindings: function(path) {

        // Find all inputs which are bound to the current input (i.e find all slaves).
        var slaves = _.reduce(this._bound, function(result, master, slave) {

            // Does the current input path starts with a master path?
            if (!path.indexOf(master)) result.push(slave);

            return result;

        }, []);

        if (!_.isEmpty(slaves)) {

            // Re-render all slave inputs
            _.each(slaves, function(slave) {
                this.renderTemplate(null, this.flatAttributes[slave], slave, { replace: true });
            }, this);

            this.trigger('render');
        }
    },

    renderTemplate: function($el, options, path, opt) {

        $el = $el || this.$el;
        opt = opt || {};

        this.resolveBindings(options);

        var value = this.getCellAttributeValue(path, options);

        var inputHtml = joint.templates.inspector[options.type + '.html']({
            options: options,
            type: options.type,
            label: options.label || path,
            attribute: path,
            value: value
        });

        // Wrap the input into a `.field` classed element so that we can easilly hide and show
        // the entire block.
        var $field = $('<div class="field"></div>').attr('data-field', path);
        var $input = $(inputHtml);
        $field.append($input);

        if (this.guard(options)) $field.addClass('hidden');

        // `options.attrs` allows for setting arbitrary attributes on the generated HTML.
        // This object is of the form: `<selector> : { <attributeName> : <attributeValue>, ... }`
        _.each(options.attrs, function(attrs, selector) {
            $field.find(selector).addBack().filter(selector).attr(attrs);
        });

        if (options.type === 'list') {
            
            _.each(value, function(itemValue, idx) {

                var $listItem = $(joint.templates.inspector['list-item.html']({
                    index: idx
                }));

                this.renderTemplate($listItem, options.item, path + '/' + idx);

                $input.children('.list-items').append($listItem);
                
            }, this);
            
        } else if (options.type === 'object') {

            options.flatAttributes = joint.util.flattenObject(options.properties, '/', function(obj) {
                // Stop flattening when we reach an object that contains the `type` property. We assume
                // that this is our options object. @TODO This is not very robust as there could
                // possibly be another object with a property `type`. Instead, we should stop
                // just before the nested leaf object.
                return obj.type;
            });

            var attributesArray = _.map(options.flatAttributes, function(options, path) {
                options.path = path;
                return options;
            });
            // Sort the attributes by `index` and assign the `path` to the `options` object
            // so that we can acess it later.
            attributesArray = _.sortBy(attributesArray, function(options) {

                return options.index;
            });

            _.each(attributesArray, function(propertyOptions) {

                var $objectProperty = $(joint.templates.inspector['object-property.html']({
                    property: propertyOptions.path
                }));

                this.renderTemplate($objectProperty, propertyOptions, path + '/' + propertyOptions.path);

                $input.children('.object-properties').append($objectProperty);
                
            }, this);
        }

        if (opt.replace) {

            $el.find('[data-field=' + path + ']').replaceWith($field);

        } else {

            $el.append($field);
        }
    },

    updateInputPosition: function() {

        var $inputX = this._byPath['position/x'];
        var $inputY = this._byPath['position/y'];

        var position = this.getModel().get('position');
        
        if ($inputX) { $inputX.val(position.x); }
        if ($inputY) { $inputY.val(position.y); }
    },
    updateInputSize: function() {

        var $inputWidth = this._byPath['size/width'];
        var $inputHeight = this._byPath['size/height'];

        var size = this.getModel().get('size');
        
        if ($inputWidth) { $inputWidth.val(size.width); }
        if ($inputHeight) { $inputHeight.val(size.height); }
    },
    updateInputAngle: function() {

        var $inputAngle = this._byPath['angle'];

        var angle = this.getModel().get('angle');
        
        if ($inputAngle) { $inputAngle.val(angle); }
    },

    onChangeInput: function(evt) {

        var $input = $(evt.target);
        var path = $input.attr('data-attribute');

        if (this.options.live) {
            
            this.updateCell($input, path);
        }

        var type = $input.attr('data-type');
        var value = this.parse(type, $input.val(), $input[0]);
        var dependants = this._when[path];

        // Notify the outside world that an input has changed.
        this.trigger('change:' + path, value, $input[0]);
        
        if (dependants) {

            // Go through all the inputs that are dependent on the value of the changed input.
            // Show them if the value of the changed input equals to their `when eq` clause value.
            // Hide them otherwise.

            _.each(dependants, function(dependant) {

                var $attribute = this._byPath[dependant.path];
                var $field = $attribute.closest('.field');
                var valid = false;

                if (dependant.regex && (new RegExp(dependant.regex)).test(value)) {
                    valid = true;
                } else if (value === dependant.value) {
                    valid = true;
                }

                if (valid) $field.removeClass('hidden');
                else $field.addClass('hidden');

            }, this);
        }
    },

    getOptions: function($attribute) {

        if ($attribute.length === 0) return undefined;
        
        var path = $attribute.attr('data-attribute');
        var type = $attribute.attr('data-type');
        var options = this.flatAttributes[path];
        if (!options) {
            var $parentAttribute = $attribute.parent().closest('[data-attribute]');
            var parentPath = $parentAttribute.attr('data-attribute');
            options = this.getOptions($parentAttribute);
            var childPath = path.replace(parentPath + '/', '');
            var parent = options;
            options = parent.item || parent.flatAttributes[childPath];
            options.parent = parent;
        }
        return options;
    },
    
    updateCell: function($attr, attrPath) {

        var cell = this.getModel();

        var byPath = {};

        if ($attr) {
            // We are updating only one specific attribute
            byPath[attrPath] = $attr;
        } else {
            // No parameters given. We are updating all attributes
            byPath = this._byPath;
        }

        _.each(byPath, function($attribute, path) {

            if ($attribute.hasClass('hidden')) return;
            
            var type = $attribute.attr('data-type');
            var value;
            var options;
            var kind;

            switch (type) {
                
              case 'list':
                // TODO: this is wrong! There could have been other properties not
                // defined in the inspector which we delete by this! We should only remove
                // those items that disappeared from DOM.
                // Note that we cannot use `this.setProperty(path, [])` without previous
                // property unset as that would not reset nested lists because of
                // the `_.merge()` operation used.
                joint.util.unsetByPath(cell.attributes, path, '/');
                this.setProperty(path, []);
                break;
                
              case 'object':
                // For objects, all is handled in the actual inputs.
                break;
                
            default:
                value = this.parse(type, $attribute.val(), $attribute[0]);
                options = this.getOptions($attribute);

                if (options.valueRegExp) {
                    var oldValue = joint.util.getByPath(cell.attributes, path, '/') || options.defaultValue;
                    value = oldValue.replace(new RegExp(options.valueRegExp), '$1' + value + '$3');
                }

                this.setProperty(path, value);
                break;
            }

            this.updateBindings(path);

        }, this);
    },

    setProperty: function(path, value) {

        var cell = this.getModel();
        
        var pathArray = path.split('/');
        var property = pathArray[0];
        var oldValue;
        
        // Make sure the inspector does not rerender itself after the `set()` call below. It is
        // not necessary as the input already contains the right value.
        var opt = {};
        opt['inspector_' + this.cid] = true;
        
        if (pathArray.length > 1) {
            // Nested object.

            var attributes = _.cloneDeep(cell.attributes);

            oldValue = joint.util.getByPath(attributes, path, '/');

            var update = {};
            // Initialize the nested object. Subobjects are either arrays or objects.
            // An empty array is created if the sub-key is an integer. Otherwise, an empty object is created.
            // Note that this imposes a limitation on object keys one can use with Inspector.
            // Pure integer keys will cause issues and are therefore not allowed.
            var initializer = update;
            var prevProperty = property;
            _.each(_.rest(pathArray), function(key) {
                initializer = initializer[prevProperty] = (_.isFinite(Number(key)) ? [] : {});
                prevProperty = key;
            });
            // Fill update with the `value` on `path`.
            update = joint.util.setByPath(update, path, value, '/');
            // Merge update with the model attributes.
            _.merge(attributes, update);
            // Finally, set the property to the updated attributes.
            cell.set(property, attributes[property], opt);

        } else {
            // Primitive object. The fast path.

            oldValue = cell.get(property);
            
            cell.set(property, value, opt);
        }
    },

    // Parse the input `value` based on the input `type`.
    // Override this method if you need your own specific parsing.
    parse: function(type, value, targetElement) {
        
        switch (type) {
          case 'number':
            value = parseFloat(value);
            break;
          case 'toggle':
            value = targetElement.checked;
            break;
          default:
            value = value;
            break;
        }
        return value;
    },

    startBatchCommand: function() {

        this.getModel().trigger('batch:start');
    },
    
    stopBatchCommand: function() {

        this.getModel().trigger('batch:stop');
    },

    addListItem: function(evt) {

        var $target = $(evt.target);
        var $attribute = $target.closest('[data-attribute]');
        var path = $attribute.attr('data-attribute');
        var options = this.getOptions($attribute);

        // Take the index of the last list item and increase it by one.
        var $lastListItem = $attribute.children('.list-items').children('.list-item').last();
        var lastIndex = $lastListItem.length === 0 ? -1 : parseInt($lastListItem.attr('data-index'), 10);
        var index = lastIndex + 1;

        var $listItem = $(joint.templates.inspector['list-item.html']({ index: index }));
        
        this.renderTemplate($listItem, options.item, path + '/' + index);

        $target.parent().children('.list-items').append($listItem);
        $listItem.find('input').focus();

        this.trigger('render');
        
        if (this.options.live) {
            this.updateCell();
        }
    },
    
    deleteListItem: function(evt) {

        var $listItem = $(evt.target).closest('.list-item');

        // Update indexes of all the following list items and their inputs.
        $listItem.nextAll('.list-item').each(function() {
            
            var index = parseInt($(this).attr('data-index'), 10);
            var newIndex = index - 1;

            // Find all the nested inputs and update their path so that it contains the new index.
            $(this).find('[data-attribute]').each(function() {
                $(this).attr('data-attribute', $(this).attr('data-attribute').replace('/' + index, '/' + newIndex));
            });

            // Update the index of the list item itself.
            $(this).attr('data-index', newIndex);
        });

        $listItem.remove();
        this.trigger('render');
        
        if (this.options.live) {
            this.updateCell();
        }
    },

    remove: function() {

        $(document).off('mouseup', this.stopBatchCommand);
        return Backbone.View.prototype.remove.apply(this, arguments);
    },

    onGroupLabelClick: function(evt) {

        // Prevent default action for iPad not to handle this event twice.
        evt.preventDefault();
        
        var $group = $(evt.target).closest('.group');
        this.toggleGroup($group.data('name'));
    },
    
    toggleGroup: function(name) {

        this.$('.group[data-name="' + name + '"]').toggleClass('closed');
    },

    closeGroup: function(name) {
        
        this.$('.group[data-name="' + name + '"]').addClass('closed');
    },

    openGroup: function(name) {
        
        this.$('.group[data-name="' + name + '"]').removeClass('closed');
    },

    closeGroups: function() {

        this.$('.group').addClass('closed');
    },

    openGroups: function() {

        this.$('.group').removeClass('closed');
    }
});
