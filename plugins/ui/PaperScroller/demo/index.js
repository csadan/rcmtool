var graph = new joint.dia.Graph;

var $app = $('#app');

var paperScroller = new joint.ui.PaperScroller;

var paper = new joint.dia.Paper({

    el: paperScroller.el,
    width: 2000,
    height: 2000,
    gridSize: 20,
    model: graph
});

paperScroller.options.paper = paper;

// Initiate panning when the user grabs the blank area of the paper.
paper.on('blank:pointerdown', paperScroller.startPanning);

paperScroller.$el.css({
    width: 300,
    height: 300
});
$app.append(paperScroller.el);

// Example of centering the paper.
paperScroller.center();


var r = new joint.shapes.basic.Rect({
    position: { x: 1020, y: 1020 },
    size: { width: 120, height: 80 },
    attrs: { text: { text: 'Rect' } }
});
graph.addCell(r);


var c = new joint.shapes.basic.Circle({
    position: { x: 1220, y: 1020 },
    attrs: { text: { text: 'Circle' } }
});
graph.addCell(c);
