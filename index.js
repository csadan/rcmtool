// These are all the exports for the NodeJS environment.
// The idea is that JointJS can be used exactly the same way in the browser and also in Node.

/**
 * Module dependencies.
 */

var express = require('express'), 
	http = require('http'), 
	path = require('path'), 
	config = require('./oauth/oauth-config'), 
	app = express(),
	MongoClient = require('mongodb').MongoClient,
	
//controladores
	Home = require('./rcmtoolControl/home'), 		//TODO: inicio plataforma
	Login = require('./rcmtoolControl/Login'),
	Logout = require('./rcmtoolControl/Logout'),
	RCMtool = require('./rcmtoolControl/RCMtool'),  //inicialización de la herramienta
	ImportGraph = require('./rcmtoolControl/importGraph'), //importación de grafo JSON
	SaveGraph = require('./rcmtoolControl/saveGraph'), 	   //almacenamiento de grafo JSON
	GetRCMNamesList = require('./rcmtoolControl/getRCMNamesList'),   //obtención de los rcms Almacenados
	getMendeleyReferences = require('./rcmtoolControl/getMendeleyReferences');
//TODO: compressor = require('./node_modules/node-minify');

// all environments
// app.set('port', process.env.PORT || 3000);
app.set('views', __dirname + '/rcmtoolView');
app.set('view engine', 'hjs');
app.use(express.favicon());
app.use(express.logger('dev'));
app.use(express.bodyParser());
app.use(express.methodOverride());
app.use(express.cookieParser('rcmtool'));
app.use(express.session());
app.use(app.router);
app.use(require('less-middleware')({
	src : __dirname + '/public'
}));
app.use(express.static(path.join(__dirname, 'public')));

var accessTokenCookieName = 'accessToken';
var refreshTokenCookieName = 'refreshToken';




// development only
if ('development' === app.get('env')) {
	app.use(express.errorHandler());
}


/*
		mode: 'local',
		port: 3000,
		mongo: {
			host: '127.0.0.1',
			port: 27017
		}
*/

MongoClient.connect('mongodb://' + '127.0.0.1' + ':' + 27017 + '/rcmtool', function(err, db) {
		if(err) {
			console.log('Sorry, there is no mongo db server running.');
		} else {
			console.log("Connected with mongodb server on port 27017 database rcmtool.")
			var attachDB = function(req, res, next) {
				req.db = db;
				next();
			};
			app.all('/',attachDB, function(req, res, next) {
				if(req.session && req.session.rcmtool && req.session.rcmtool === true){
					console.log(req.session.rcmtool)
					Home.run(req,res,next);
				}else{
					res.redirect("/login")
				}
				//res.redirect('/rcmtool');
			});
			
			app.all('/login',attachDB, function(req, res, next) {
				if(req.session && req.session.rcmtool && req.session.rcmtool === true){
					res.redirect("/")
				}else{
					Login.run(req,res,next);
				}
			});

			app.all('/logout',attachDB, function(req, res, next) {
				if(req.session && req.session.rcmtool && req.session.rcmtool === true){
					Logout.run(req,res,next);
				}else{
					res.redirect("/login");
				}
			});

			require('./oauth/oauth-app')(app, config);
			require('./routingRCMGraph/rcmEdit-app')(app,db);
			
			var httpServer = http.createServer(app);
			httpServer.listen(19313);

			console.log("app listening in port 19313");
		}
	});