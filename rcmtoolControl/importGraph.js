//
///*
//Vista principal que recoge las peticiones GET y POST
//recurso: / y /home
//vista:home.hjs
//*/
//var BaseController = require("./Base"),
//	View = require("../rcmtoolView/Base");
//
//module.exports = BaseController.extend({ 
//	name: "importGraph",
//	content: null,
//	run: function(req, res, next) {
//		console.log("enviando grafo: "+this.model.getGraph(req.body.name))
//		res.send(this.model.getGraph(req.body.name));
//	},
//});

var BaseController = require("./Base"),
	View = require("../rcmtoolView/Base"),
	modelDB = new (require("../rcmtoolModel/modelDB"));

module.exports = BaseController.extend({ 
	name: "importGraph",
	content: null,
	run: function(req, res, next) {
		modelDB.setDB(req.db);
		modelDB.setCollection('usuarios');
		var self = this;
		modelDB.getlistWithOptions(function(err, rtd) {
			console.log(req.session.uid);
			console.log(req.body.name)
			if(rtd[0].rcms ===0) {
				console.log("no existe rcm con ese nombre")
				res.send('')	
			}else {
				res.send(rtd[0].rcms[0].model);
			}
		},{'username':req.session.uid,'rcms': { $elemMatch: { name: req.body.name }}},{"_id":0,"rcms.$":1})
	}
});




















