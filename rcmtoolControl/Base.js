var _ = require("underscore");
Model= require("../rcmtoolModel/model.js");

module.exports = {
	name: "base",
	model: new Model(),
	extend: function(child) {
		return _.extend({}, this, child);
	},
	run: function(req, res, next) {
	}
}