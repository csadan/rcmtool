
/*
Vista principal que recoge las peticiones GET y POST
recurso: / y /home
vista:home.hjs
*/
var BaseController = require("./Base"),
	View = require("../rcmtoolView/Base");

var oauthPath = '/oauth';

module.exports = BaseController.extend({ 
	name: "getMendeleyReferences",
	content: null,
	run: function(req, res, next) {
		console.log("entra en el controlador")
		res.redirect(oauthPath);
	}
});


/*
			reflist=reflist + '<a href="javascript:void(0)">\
			<div class="reference" id="'+(i+1)+'" onclick="">\
				<div class="colLeft">\
					<div class="idRef">['+(i+1)+']</div>\
					<div class="yearRef">'+documents[i].year+'</div>\
				</div>\
				<div class="colRight">\
                    <div class="titleRef">'+documents[i].title+'</div>\
                    <div class="authRef">'+documents[i].authors[0].first_name+' '+documents[i].authors[0].last_name+'</div>\
				</div>\
			</div>\
			</a>'
*/