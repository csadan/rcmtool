/*
controlador para el login, recoge las peticiones GET y POST
recurso: /login
vista: Login.hjs
*/

var BaseController = require("./Base"),
	View = require("../rcmtoolView/Base"),
	modelDB = new (require("../rcmtoolModel/modelDB"));

module.exports = BaseController.extend({ 
	name: "logout",
	content: null,
	run: function(req, res, next) {
		var self = this;
		if( req.session && req.session.rcmtool && req.session.rcmtool === true){
				req.session.destroy();
		}
		res.redirect('/login')
	}
});



