
/*
Vista principal que recoge las peticiones GET y POST
recurso: / y /home
vista:home.hjs
*/
var BaseController = require("./Base"),
	View = require("../rcmtoolView/Base"),
	modelDB = new (require("../rcmtoolModel/modelDB"));


module.exports = BaseController.extend({ 
	name: "home",
	content: null,
	run: function(req, res, next) {
		var v = new View(res, 'index');
		v.render();
	},
});



















