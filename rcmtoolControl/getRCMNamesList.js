/*
var BaseController = require("./Base"),
View = require("../rcmtoolView/Base");

module.exports = BaseController.extend({ 
	name: "getRCMList",
	content: null,
	run: function(req, res, next) {
		//console.log("RCMs: "+this.model.getRCMList())
		res.send(this.model.getRCMList());
	}
});
*/

/*
controlador para el login, recoge las peticiones GET y POST
recurso: /login
vista: Login.hjs
*/
var BaseController = require("./Base"),
	View = require("../rcmtoolView/Base"),
	modelDB = new (require("../rcmtoolModel/modelDB"));

module.exports = BaseController.extend({ 
	name: "getRCMNamesList",
	content: null,
	run: function(req, res, next) {
		modelDB.setDB(req.db);
		modelDB.setCollection('usuarios');
		var self = this;
		modelDB.getlistWithOptions(function(err, rtd) {
			if(rtd.length ===0) {
				res.send('')	
			}else {
				var response = [];
				for (var i = 0; i < rtd[0].rcms.length; i++) {
					response.push(rtd[0].rcms[i].name);
				}
				res.send(response.join())
			}
		}, {'username':req.session.uid}, {'rcms.name' : 1,'_id':0})
	}
});

