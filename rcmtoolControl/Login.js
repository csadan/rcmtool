/*
controlador para el login, recoge las peticiones GET y POST
recurso: /login
vista: Login.hjs
*/

var BaseController = require("./Base"),
	View = require("../rcmtoolView/Base"),
	modelDB = new (require("../rcmtoolModel/modelDB"));

module.exports = BaseController.extend({ 
	name: "login",
	content: null,
	run: function(req, res, next) {
		modelDB.setDB(req.db);
		modelDB.setCollection('usuarios');

		var self = this;
		if(req.session && req.session.rcmtool && req.session.rcmtool === true){
			var v = new View(res, 'index');
			v.render();
		}else{
			var usr= req.body.username;
			var psw=req.body.password;
			if(usr && psw){
				modelDB.getlist(function(err, records) {
					if(records.length ===0) {
						console.log("Usuario No Existente")
						var v = new View(res, 'login_new');
						v.render({
							title:'Please login'
						});
					}else {
						console.log("[INFO] Usuario existente: "+req.body.username)
						req.session.rcmtool= true;
						req.session.uid=records[0].username;
						req.session.save();
						res.redirect('/')
					}
				}, {password:psw, username:usr});
			}else{
				var v = new View(res, 'login_new');
				v.render({
					title:'Please login'
				});
			}
		}
	}
});



