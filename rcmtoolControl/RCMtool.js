
/*
Vista principal que recoge las peticiones GET y POST
recurso: / y /home
vista:home.hjs
*/
var BaseController = require("./Base"),
	View = require("../rcmtoolView/Base");

var accessTokenCookieName = 'accessToken';
var refreshTokenCookieName = 'refreshToken';

module.exports = BaseController.extend({ 
	name: "RCMtool",
	content: null,
	run: function(req, res, next) {
			var v = new View(res, 'rcmtool'); //
			v.render();
	},
});