/*
var BaseController = require("./Base"),
View = require("../rcmtoolView/Base");

module.exports = BaseController.extend({ 
	name: "getRCMList",
	content: null,
	run: function(req, res, next) {
		//console.log("RCMs: "+this.model.getRCMList())
		res.send(this.model.getRCMList());
	}
});
*/

/*
controlador para el login, recoge las peticiones GET y POST
recurso: /login
vista: Login.hjs
*/
var BaseController = require("./Base"),
	View = require("../rcmtoolView/Base"),
	modelDB = new (require("../rcmtoolModel/modelDB"));

module.exports = BaseController.extend({ 
	name: "getRCMList",
	content: null,
	run: function(req, res, next) {
		modelDB.setDB(req.db);
		modelDB.setCollection('usuarios');
		var self = this;
		modelDB.getlistWithOptions(function(err, rtd) {
			console.log(rtd)
			if(rtd[0].rcms.length ===0) {
				console.log("no existen rcms");
				res.send('');
			}else {
				var response = [];
				for (var i = 0; i < rtd[0].rcms.length; i++) {
					response.push({'name':rtd[0].rcms[i].name, 'description': rtd[0].rcms[i].description, 'date': rtd[0].rcms[i].date});
				}
				res.send(JSON.stringify(response));
			}
		}, {'username':req.session.uid},{'rcms' : 1,'_id':0})
	}
});








