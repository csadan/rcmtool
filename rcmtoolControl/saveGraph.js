//
///*
//Vista principal que recoge las peticiones GET y POST
//recurso: / y /home
//vista:home.hjs
//*/
//var BaseController = require("./Base"),
//View = require("../rcmtoolView/Base");
//
//module.exports = BaseController.extend({ 
//	name: "saveGraph",
//	content: null,
//	run: function(req, res, next) {
//		console.log("->name: "+req.body.name+"\n"+"recibido: "+req.body.jsonString)
//		this.model.setGraph(req.body.name,req.body.jsonString);
//		res.send("DONE")
//	},
//});


var BaseController = require("./Base"),
	View = require("../rcmtoolView/Base"),
	modelDB = new (require("../rcmtoolModel/modelDB"));

module.exports = BaseController.extend({ 
	name: "saveGraph",
	content: null,
	run: function(req, res, next) {
	    var date = new Date();

	    var hour = date.getHours();
	    hour = (hour < 10 ? "0" : "") + hour;

	    var min  = date.getMinutes();
	    min = (min < 10 ? "0" : "") + min;

	    var year = date.getFullYear();

	    var month = date.getMonth() + 1;
	    month = (month < 10 ? "0" : "") + month;

	    var day  = date.getDate();
	    day = (day < 10 ? "0" : "") + day;
		
		
		modelDB.setDB(req.db);
		modelDB.setCollection('usuarios')
		var self = this;
		console.log(req.session.uid)
		modelDB.update({"username": req.session.uid},
				 { $push: { "rcms": {
										name:req.body.name,
										model:req.body.jsonString,
										date: {"day" : day,"month" : month,"year" : year,"hour": hour+':'+min}
                        			}
                          }
                },function(err,rtds){
					if(!err) console.log("añadido rcm: "+req.body.name)
                });		

//		modelDB.getlist(function(err, records) {
//			if(records.length===0){
//				modelDB.insert(
//						{
//							name:req.body.name,
//							model:req.body.jsonString,
//							date: {"day" : day,"month" : month,"year" : year,"hour": hour+':'+min}
//						},function(err,data){
//									if (!err) {
//										console.log("añadido rcm: "+req.body.name)
//									}else{
//										console.log("error al insertar rcm")
//									}
//						});
//				res.send("OK")
//			}else{
//				res.send("RCM already exist.");
//			}	
//		}, { name:req.body.name});

	}
});