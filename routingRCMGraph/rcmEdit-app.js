RCMtool = require('../rcmtoolControl/RCMtool'),  //inicialización de la herramienta
ImportGraph = require('../rcmtoolControl/importGraph'), //importación de grafo JSON
SaveGraph = require('../rcmtoolControl/saveGraph'),     //almacenamiento de grafo JSON
GetRCMNamesList = require('../rcmtoolControl/getRCMNamesList'),   //obtención de los rcms Almacenados
GetRCMList = require('../rcmtoolControl/getRCMList'),
getMendeleyReferences = require('../rcmtoolControl/getMendeleyReferences');
EditRCM = require('../rcmtoolControl/editRCM'),  //inicialización de la herramienta




/*jshint camelcase: false */
module.exports = function(app,db) {

    'use strict';
    
    
    var attachDB = function(req, res, next) {
    	req.db = db;
    	next();
    };

    // controlador de página de inicio
    app.all('/rcmtool',attachDB, function(req, res, next) {
		if(req.session && req.session.rcmtool && req.session.rcmtool === true){
	        RCMtool.run(req, res, next);
		}else{
	        res.redirect("/login");
		}
    });

    app.all('/importGraph',attachDB, function(req, res, next) {
		if(req.session && req.session.rcmtool && req.session.rcmtool === true){
	        ImportGraph.run(req, res, next);
		}else{
	        res.redirect("/login");
		}
    });

    app.all('/saveGraph',attachDB, function(req, res, next) {
		if(req.session && req.session.rcmtool && req.session.rcmtool === true){
	        SaveGraph.run(req, res, next);
		}else{
			res.redirect("/login");
		}
    });

    app.all('/getRCMNamesList',attachDB, function(req, res, next) {
		if(req.session && req.session.rcmtool && req.session.rcmtool === true){
	    	GetRCMNamesList.run(req, res, next);
		}else{
			res.redirect("/login");			
		}    	
    });
    
    app.all('/getRCMList',attachDB, function(req, res, next) {
		if(req.session && req.session.rcmtool && req.session.rcmtool === true){
	    	GetRCMList.run(req, res, next);
		}else{
			res.redirect("/login");
		}      	
    });
    
    app.get('/editRCM/:rcmname', function(req, res,next) {
		if(req.session && req.session.rcmtool && req.session.rcmtool === true){
	    	EditRCM.run(req, res, next);
		}else{
			res.redirect("/login");
		}  
    });
    
    
};
